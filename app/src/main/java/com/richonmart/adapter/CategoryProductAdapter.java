package com.richonmart.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.BaseAdapter;
import com.richonmart.model.Category;
import com.richonmart.model.Order;
import com.richonmart.utils.Extension;

public class CategoryProductAdapter extends BaseAdapter<Category, CategoryProductAdapter.ViewHolder> {

    private CategoryProductAdapter.OnItemClickListener mListener;
    private Activity context;
    private boolean isHomeCategory;

    public CategoryProductAdapter(Activity context, boolean isHomeCategory, CategoryProductAdapter.OnItemClickListener listener) {
        this.mListener = listener;
        this.context = context;
        this.isHomeCategory = isHomeCategory;
    }

    @Override
    protected CategoryProductAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new CategoryProductAdapter.ViewHolder(inflater.inflate(R.layout.item_product_category, parent, false));
    }

    public interface OnItemClickListener {
        void onClick(View view, int position, Category category);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryProductAdapter.ViewHolder holder, final int position) {
        final Category item = getItem(position);
        Extension.setImage(context, holder.ivCategory, item.getCoverUrl());
        holder.tvCategoryName.setText(item.getTitle());
        holder.cvCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(v, position, item);
            }
        });
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (!isHomeCategory) {

            int width = displayMetrics.widthPixels;

            float calculatedSize = width / 2f - 50;
            float height = calculatedSize / 1.7f;
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(Integer.parseInt(String.valueOf(Math.round(calculatedSize))), Integer.parseInt(String.valueOf(Math.round(height))));
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(20, 20, 20, 8);
            holder.cvCategory.setLayoutParams(layoutParams);
        }
    }

    public class ViewHolder extends BaseAdapter.BaseViewHolder {
        @BindView(R.id.tvCategoryName)
        TextView tvCategoryName;
        @BindView(R.id.ivCategory)
        ImageView ivCategory;
        @BindView(R.id.cvCategory)
        CardView cvCategory;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
