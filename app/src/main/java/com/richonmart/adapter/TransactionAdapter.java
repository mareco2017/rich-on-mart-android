package com.richonmart.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.base.BaseAdapter;
import com.richonmart.model.Order;
import com.richonmart.utils.Extension;

public class TransactionAdapter extends BaseAdapter<Order, TransactionAdapter.OrderVH> {

    private Activity context;
    private TransactionAdapter.OnItemClickListener mListener;
    private int type = 1;


    public TransactionAdapter(Activity context, int type, TransactionAdapter.OnItemClickListener listener) {
        this.context = context;
        this.type = type;
        this.mListener = listener;
    }

    public interface OnItemClickListener {
        void onClick(View view, int position, Order order);
    }

    @Override
    protected TransactionAdapter.OrderVH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new TransactionAdapter.OrderVH(inflater.inflate(R.layout.item_transaction, parent, false));
    }

    public void onBindViewHolder(final @NonNull TransactionAdapter.OrderVH holder, final int position) {
        final Order order = getItem(position);
        holder.tvInvoiceNo.setText(order.getReferenceNumber());
        holder.tvDate.setText(Extension.receiptDetailDateFormat.format(order.getCreatedAt()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(v, position, order);
            }
        });
        if (order.getStatus() == 20) {
            holder.tvStatus.setText("Pesanan Terkonfirmasi");
        }
        if (order.getStatus() == 21) {
            holder.tvStatus.setText("Pesanan Dibelanja");
        }
        if (order.getStatus() == 22) {
            holder.tvStatus.setText("Pesanan Diantar");
        }
        if (order.getStatus() == 3) {
            holder.tvStatus.setText("Pesanan Selesai");
        }
    }


    public class OrderVH extends BaseAdapter.BaseViewHolder {
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvInvoiceNo)
        TextView tvInvoiceNo;

        private OrderVH(View itemView) {
            super(itemView);
        }
    }

}
