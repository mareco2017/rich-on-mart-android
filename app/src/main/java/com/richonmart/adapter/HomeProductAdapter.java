package com.richonmart.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.base.BaseAdapter;
import com.richonmart.model.Product;
import com.richonmart.utils.Extension;

public class HomeProductAdapter extends BaseAdapter<Product, HomeProductAdapter.ProductVH> {

    private Activity context;
    private HomeProductAdapter.OnItemClickListener mListener;
    private int type = 1;


    public HomeProductAdapter(Activity context, int type, HomeProductAdapter.OnItemClickListener listener) {
        this.context = context;
        this.type = type;
        this.mListener = listener;
    }

    public interface OnItemClickListener {
        void onClick(View view, int position, Product product);
    }

    @Override
    protected HomeProductAdapter.ProductVH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        if (type == 1) {
            return new HomeProductAdapter.ProductVH(inflater.inflate(R.layout.item_product, parent, false));
        }
        else {
            return new HomeProductAdapter.ProductVH(inflater.inflate(R.layout.item_product_cart, parent, false));
        }
    }

    public void onBindViewHolder(final @NonNull HomeProductAdapter.ProductVH holder, final int position) {
        final Product product = getItem(position);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        float calculatedSize = width / 2f;
        float height = calculatedSize / 1.7f;
        if (type == 1){
            calculatedSize = width /3f;
        }
        else {
            calculatedSize = width /1.7f;
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(Integer.parseInt(String.valueOf(Math.round(calculatedSize))), Integer.parseInt(String.valueOf(Math.round(height))));
        layoutParams.gravity = Gravity.CENTER;
        holder.ivProduct.setLayoutParams(layoutParams);
        holder.tvPrice.setText(Extension.priceFormat(product.getCalcutaltedPrice()));
        holder.tvPrice2.setText(Extension.priceFormat(product.getPrice()));
        if (product.getDiscount() == 0) {
            holder.tvPrice2.setText(" ");
            holder.llPromo.setVisibility(View.GONE);
        }
        else {
            holder.tvPrice2.setVisibility(View.VISIBLE);
            holder.llPromo.setVisibility(View.VISIBLE);
        }
        Extension.setImage(context, holder.ivProduct, product.getCoverUrl());
        holder.tvProductName.setText(product.getTitle());
        holder.tvUnit.setText(product.getUnitString());
        String discountString = "Hemat " + Extension.priceFormat(product.getDiscount());
        holder.tvDiscount.setText(discountString);
        if (holder.rlAddtoCart != null) {
            holder.rlAddtoCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v,1,product);
                }
            });
        }
        if (type == 2) {
            holder.tvQty.setText(String.valueOf(product.getCartQty()));
            holder.ibRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v, position, product);
                }
            });
            holder.ivDecreaseQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (product.getCartQty() == 1) {
                        mListener.onClick(v, position, product);
                    }
                    else {
                        API.updateProductQty(product, -1);
                        int qty = product.getCartQty() - 1;
                        product.setCartQty(qty);
                        holder.tvQty.setText(String.valueOf(qty));
                        mListener.onClick(v,-1,product);
                    }
                }
            });
            holder.ivAddQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    API.updateProductQty(product, 1);
                    int qty = product.getCartQty() + 1;
                    product.setCartQty(qty);
                    holder.tvQty.setText(String.valueOf(qty));
                    mListener.onClick(v,-1,product);
                }
            });
        }
        else if(type == 3) {
            holder.rlAddtoCart.setVisibility(View.VISIBLE);
            holder.llQty.setVisibility(View.GONE);
            holder.ibRemove.setVisibility(View.GONE);
            holder.rlAddtoCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v, position, product);
                }
            });
        }
    }

//    private void updateQty()

    public class ProductVH extends BaseAdapter.BaseViewHolder {
        @BindView(R.id.ivProduct)
        ImageView ivProduct;
        @BindView(R.id.tvPrice)
        TextView tvPrice;
        @BindView(R.id.tvPrice2)
        TextView tvPrice2;
        @BindView(R.id.tvProductName)
        TextView tvProductName;
        @BindView(R.id.llPromo)
        LinearLayout llPromo;
        @BindView(R.id.tvUnit)
        TextView tvUnit;
        @BindView(R.id.tvDiscount)
        TextView tvDiscount;
        @Nullable
        @BindView(R.id.rlAddtoCart)
        RelativeLayout rlAddtoCart;
        @Nullable
        @BindView(R.id.tvQty)
        TextView tvQty;
        @Nullable
        @BindView(R.id.ibRemove)
        ImageButton ibRemove;
        @Nullable
        @BindView(R.id.ivAddQty)
        ImageView ivAddQty;
        @Nullable
        @BindView(R.id.ivDecreaseQty)
        ImageView ivDecreaseQty;
        @Nullable
        @BindView(R.id.llQty)
        LinearLayout llQty;

        private ProductVH(View itemView) {
            super(itemView);
        }
    }

}
