package com.richonmart.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.BaseAdapter;
import com.richonmart.model.Address;
import com.richonmart.model.Address;
import com.richonmart.utils.Extension;

public class AddressAdapter  extends BaseAdapter<Address, AddressAdapter.ViewHolder> {

    private AddressAdapter.OnItemClickListener mListener;
    private Activity context;
    private boolean isFromCart;

    public AddressAdapter(Activity context, boolean isFromCart, AddressAdapter.OnItemClickListener listener) {
        this.mListener = listener;
        this.context = context;
        this.isFromCart = isFromCart;
    }

    @Override
    protected AddressAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new AddressAdapter.ViewHolder(inflater.inflate(R.layout.item_address, parent, false));
    }

    public interface OnItemClickListener {
        void onClick(View view, int position, Address Address);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddressAdapter.ViewHolder holder, final int position) {
        final Address item = getItem(position);
        if(isFromCart) {
            holder.ivMore.setVisibility(View.GONE);
            holder.tvPrimary.setVisibility(View.GONE);
            holder.llAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v, position, item);
                }
            });
        }
        else {
            if (item.isPrimary() == 1) {
                holder.tvPrimary.setVisibility(View.VISIBLE);
            }
            else {
                holder.tvPrimary.setVisibility(View.GONE);
            }
            holder.ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v, position, item);
                }
            });
        }
        holder.tvName.setText(item.getName());
        holder.tvPhoneNumber.setText(item.getPhoneNumber());
        holder.tvAddress.setText(item.getFullAddress()+ "\n" + item.getSubDistrict()+ ", "+ item.getCity() + "\n" + item.getProvince()+", "+ item.getPostalCode());
        holder.tvPhoneNumber.setText(item.getPhoneNumber());

    }

    public class ViewHolder extends BaseAdapter.BaseViewHolder {
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPrimary)
        TextView tvPrimary;
        @BindView(R.id.tvRecipientName)
        TextView tvRecipientName;
        @BindView(R.id.tvPhoneNumber)
        TextView tvPhoneNumber;
        @BindView(R.id.llAddress)
        LinearLayout llAddress;
        @BindView(R.id.ivMore)
        ImageView ivMore;

        public ViewHolder(View view) {
            super(view);
        }
    }
}
