package com.richonmart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.BaseAdapter;
import com.richonmart.model.PaymentProduct;
import com.richonmart.utils.Extension;

public class MobileCreditAdapter extends BaseAdapter<PaymentProduct, MobileCreditAdapter.ViewHolder> {

    private MobileCreditAdapter.OnItemClickListener mListener;
    private Context context;
    private int type;

    public MobileCreditAdapter(Context context, MobileCreditAdapter.OnItemClickListener listener, int type) {
        this.mListener = listener;
        this.context = context;
        this.type = type;
    }

    @Override
    protected MobileCreditAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        if (type == 1 || type == -1) {
            return new MobileCreditAdapter.ViewHolder(inflater.inflate(R.layout.item_phone_credit, parent, false));
        }
        else {
            return new MobileCreditAdapter.ViewHolder(inflater.inflate(R.layout.item_pascabayar, parent, false));
        }
    }

    public interface OnItemClickListener {
        void onClick(View view, int position, PaymentProduct exploreCategory);
    }

    @Override
    public void onBindViewHolder(@NonNull final MobileCreditAdapter.ViewHolder holder, final int position) {
        final PaymentProduct item = getItem(position);
        holder.tvName.setText(item.getTitle());
        if (type == 1 || type == -1) {
            holder.tvName.setText(Extension.priceFormat(item.getFinalPrice()));
            holder.tvPrice.setText(item.getTitle());
        }
        else {
            Extension.setImage(context, holder.ivProduct, item.getIconUrl());
        }
        holder.btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(v, position, item);
            }
        });
    }

    public class ViewHolder extends BaseAdapter.BaseViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        @Nullable
        @BindView(R.id.tvPrice)
        TextView tvPrice;
        @BindView(R.id.btnSelect)
        TextView btnSelect;
        @Nullable
        @BindView(R.id.ivProduct)
        ImageView ivProduct;

        public ViewHolder(View view) {
            super(view);
        }
    }
}

