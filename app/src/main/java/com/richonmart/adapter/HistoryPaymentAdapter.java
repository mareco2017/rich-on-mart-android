package com.richonmart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import com.google.android.gms.vision.text.Line;
import com.richonmart.R;
import com.richonmart.base.BaseAdapter;
import com.richonmart.model.Order;
import com.richonmart.model.PaymentProduct;
import com.richonmart.utils.Extension;

public class HistoryPaymentAdapter extends BaseAdapter<Order, HistoryPaymentAdapter.ViewHolder> {

    private HistoryPaymentAdapter.OnItemClickListener mListener;
    private Context context;
    private int type;

    public HistoryPaymentAdapter(Context context, HistoryPaymentAdapter.OnItemClickListener listener, int type) {
        this.mListener = listener;
        this.context = context;
        this.type = type;
    }

    @Override
    protected HistoryPaymentAdapter.ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new HistoryPaymentAdapter.ViewHolder(inflater.inflate(R.layout.item_history_payment, parent, false));
    }

    public interface OnItemClickListener {
        void onClick(View view, int position, Order order);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryPaymentAdapter.ViewHolder holder, final int position) {
        final Order item = getItem(position);
        holder.tvInvoiceNo.setText(item.getReferenceNumber());
        holder.tvAmount.setText(Extension.priceFormat(item.getTotal()));
        if (type == 11) {
            holder.tvType.setText(R.string.air_atb);
        }
        if (type == 17) {
            holder.tvType.setText(R.string.tagihan_listrik);
        }
        else if (type == 12 || type == 15 || type == 16) {
            holder.tvType.setText(item.getOptions().getInquiry().getProductCode());
        }
        else {
            if (item.getOrderDetail().size() > 0) {
                holder.tvType.setText(item.getOrderDetail().get(0).getProductable().getTitle());
            }
        }
        holder.llStatus.setVisibility(item.getStatus() == 2 ? View.VISIBLE : View.GONE);
        holder.tvDate.setText(Extension.receiptDetailDateFormat.format(item.getCreatedAt()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(v, position, item);
            }
        });
    }

    public class ViewHolder extends BaseAdapter.BaseViewHolder {
        @BindView(R.id.tvInvoiceNo)
        TextView tvInvoiceNo;
        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvType)
        TextView tvType;
        @BindView(R.id.llStatus)
        LinearLayout llStatus;

        public ViewHolder(View view) {
            super(view);
        }
    }
}

