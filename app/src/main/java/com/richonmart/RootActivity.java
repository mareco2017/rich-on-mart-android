package com.richonmart;

import android.content.Intent;
import android.os.Bundle;
import com.richonmart.activity.MainActivity;
import com.richonmart.activity.RegisterActivity;
import com.richonmart.activity.SplashActivity;
import com.richonmart.base.BaseActivity;
import com.richonmart.fragment.LoginFragment;

public class RootActivity extends BaseActivity {

    @Override
    protected int getContentViewResource() {
        return 0;
    }

    @Override
    protected void onViewCreated() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }
}