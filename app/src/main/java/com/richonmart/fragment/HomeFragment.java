package com.richonmart.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.orhanobut.hawk.Hawk;
import com.richonmart.activity.*;
import com.richonmart.activity.payment_product.*;
import com.richonmart.adapter.CategoryProductAdapter;
import com.richonmart.adapter.MobileCreditAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.api.MarecoAPI;
import com.richonmart.model.*;
import com.richonmart.R;
import com.richonmart.adapter.HomeProductAdapter;
import com.richonmart.base.BaseFragment;
import com.richonmart.utils.BannerImageLoader;
import com.richonmart.utils.Extension;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.banner)
    com.youth.banner.Banner banner;

    @BindView(R.id.llWallet)
    LinearLayout llWallet;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;

    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;

    @BindView(R.id.llProductPromo)
    LinearLayout llProductPromo;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    private HomeProductAdapter mAdapter;

    private CategoryProductAdapter categoryAdapter;

    private ArrayList<Product> promotionProducts = new ArrayList<>();
    private List<Banner> bannerList = new ArrayList<>();

    @Override
    protected int getContentViewResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void onViewCreated() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        double height = displayMetrics.heightPixels / 4.3;
        banner.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) height));
//        setupBanner();
        getBanner();
        Extension.setupWallet(llWallet,tvBalance, getActivity());
        if (MarecoAPI.isLoggedIn()) {
            getMarecoProfile();
        }
        swipeRefresh.setColorSchemeResources(R.color.primary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               getBanner();
                if (MarecoAPI.isLoggedIn()) {
                    getMarecoProfile();
                }
            }
        });

        rvProduct.setLayoutManager(new LinearLayoutManager(getContext(), GridLayoutManager.HORIZONTAL, false));
        mAdapter = new HomeProductAdapter(getActivity(), 1, new HomeProductAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position, Product product) {
                API.addCartProduct(product);
                Toast.makeText(getActivity(), "Sukses ditambahkan ke cart", Toast.LENGTH_SHORT).show();
            }
        });
        rvProduct.setAdapter(mAdapter);

        rvCategory.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false));
        categoryAdapter = new CategoryProductAdapter(getActivity(), true, new CategoryProductAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position, Category category) {
                Intent intent = new Intent(getContext(), ProductListActivity.class);
                intent.putExtra("Category", category);
                startActivity(intent);
            }
        });
        rvCategory.setAdapter(categoryAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Extension.setupWallet(llWallet,tvBalance, getActivity());
    }

    private void getBanner() {
        try {
            API.service().getHomeBanner().enqueue(new APICallback<APIResponse>(getContext()) {
                @Override
                protected void onSuccess(APIResponse response) {
                    if (swipeRefresh != null) {
                        swipeRefresh.setRefreshing(false);
                    }
                    onDataReceived(response.getData());
                    llProductPromo.setVisibility(response.getData().getProducts().size() > 0 ? View.VISIBLE : View.GONE);
                    mAdapter.setItems(response.getData().getProducts());
                    categoryAdapter.setItems(response.getData().getCategories());
                }

                @Override
                protected void onError(BadRequest error) {
                    swipeRefresh.setRefreshing(false);
                }
            });
        } catch (Exception exception) {
            Log.e("fetchProfile", "" + exception);
        }
    }

    public void onDataReceived(APIModels response) {
        try {
            bannerList = response.getBanners();
            List<String> images = new ArrayList<>();
            for (Banner banner : bannerList) {
                images.add(banner.getCoverUrl());
            }

            banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
            banner.setIndicatorGravity(BannerConfig.CENTER);
            // set the image loader
            banner.setImageLoader(new BannerImageLoader());
            //Set the picture collection
            banner.setImages(images);
            //Set banner animation
            banner.setBannerAnimation(Transformer.Default);
            // set the automatic rotation, the default is to true
            banner.isAutoPlay(true);
            // Set rotation time
            banner.setDelayTime(10000);
            // set the pointer position (when there banner mode indicator)
            banner.start();
            banner.setOnBannerListener(new OnBannerListener() {
                @Override
                public void OnBannerClick(int position) {
                }
            });
        } catch (Exception exception) {
            Log.e("ONRECEIVE_BANNER", "" + exception);
        }
    }

    private void getMarecoProfile() {
        MarecoAPI.service().getMarecoUserProfile().enqueue(new APICallback<APIResponse>(getContext(), 2) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                MarecoAPI.setUser(apiResponse.getData().getUser());
                Extension.setupWallet(llWallet,tvBalance, getActivity());
            }

            @Override
            protected void onError(BadRequest error) {

            }
        });
    }

    private void setupBanner() {
        List<String> images = new ArrayList<>();
        images.add("http://vanderbiltpoliticalreview.com/wp-content/uploads/iStock-589415708-808x454.jpg");
        images.add("http://vanderbiltpoliticalreview.com/wp-content/uploads/iStock-589415708-808x454.jpg");
        images.add("http://vanderbiltpoliticalreview.com/wp-content/uploads/iStock-589415708-808x454.jpg");

        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        banner.setIndicatorGravity(-10);
        // set the image loader
        banner.setImageLoader(new BannerImageLoader());
        //Set the picture collection
        banner.setImages(images);
        //Set banner animation
        banner.setBannerAnimation(Transformer.Default);
        // set the automatic rotation, the default is to true
        banner.isAutoPlay(true);
        // Set rotation time
        banner.setDelayTime(10000);
        // set the pointer position (when there banner mode indicator)
        banner.start();
        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
            }
        });
    }

    @OnClick({R.id.llAtb, R.id.llPulsa, R.id.llPstn, R.id.llPascabayar, R.id.llInternet, R.id.llPln, R.id.llPacketData})
    void ppobClicked(View view) {
        if (!API.isLoggedIn()) {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setMessage("Silahkan Login terlebih dahulu");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }
        if (!MarecoAPI.isLoggedIn()) {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setMessage("Silahkan aktifkan ROP  Anda terlebih dahulu");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return;
        }
        switch (view.getId()) {
            case R.id.llAtb: {
                Intent newIntent = new Intent(getActivity(), WaterBillActivity.class);
                startActivity(newIntent);
                break;
            }
            case R.id.llPulsa: {
                Intent newIntent = new Intent(getActivity(), PhoneAndDataActivity.class);
                startActivity(newIntent);
                break;
            }
            case R.id.llPstn: {
                Intent newIntent = new Intent(getActivity(), PascabayarActivity.class);
                newIntent.putExtra("TYPE", 3);
                startActivity(newIntent);
                break;
            }
            case R.id.llPascabayar: {
                Intent newIntent = new Intent(getActivity(), PascabayarActivity.class);
                newIntent.putExtra("TYPE", 1);
                startActivity(newIntent);
                break;
            }
            case R.id.llPln: {
                Intent newIntent = new Intent(getActivity(), PayPLNActivity.class);
                newIntent.putExtra("TYPE", 1);
                startActivity(newIntent);
                break;
            }
            case R.id.llInternet: {
                Intent newIntent = new Intent(getActivity(), PascabayarActivity.class);
                newIntent.putExtra("TYPE", 4);
                startActivity(newIntent);
                break;
            }
            case R.id.llPacketData: {
                Intent newIntent = new Intent(getActivity(), PhoneAndDataActivity.class);
                newIntent.putExtra("TYPE", -1);
                startActivity(newIntent);
                break;
            }
        }
    }

    @OnClick(R.id.tvDetail)
    void topTopUpInstruction() {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        intent.putExtra("TYPE", "top_up_instruction");
        startActivity(intent);
    }

    @OnClick(R.id.tvSeeAllCategory)
    void toCategoryList() {
        Intent intent = new Intent(getActivity(), CategoryListActivity.class);
        startActivity(intent);
    }


}