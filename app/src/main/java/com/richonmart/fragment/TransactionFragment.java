package com.richonmart.fragment;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.activity.TransactionDetailActivity;
import com.richonmart.activity.payment_product.HistoryDetailActivity;
import com.richonmart.activity.payment_product.PaymentHistoryActivity;
import com.richonmart.adapter.HomeProductAdapter;
import com.richonmart.adapter.TransactionAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.BaseFragment;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Order;
import com.richonmart.model.Product;

import java.util.ArrayList;
import java.util.List;

public class TransactionFragment  extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private TransactionAdapter mAdapter;

    private ArrayList<Product> promotionProducts = new ArrayList<>();

    @Override
    protected int getContentViewResource() {
        return R.layout.fragment_order_history;
    }

    @Override
    protected void onViewCreated() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), GridLayoutManager.VERTICAL, false));
        mAdapter = new TransactionAdapter(getActivity(), 2, new TransactionAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, final int position, Order order) {
                Intent intent = new Intent(getActivity(), TransactionDetailActivity.class);
                intent.putExtra("Order", order);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(mAdapter);
        if (API.isLoggedIn()) {
            getOrder();
        }
    }

    private void getOrder() {
        API.service().getOrder(80,0,50, "20,21,22").enqueue(new APICallback<APIResponse>(getActivity()) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                List<Order> orders = apiResponse.getData().getOrders();
                if (orders.size() > 0) {
                    mAdapter.setItems(orders);
                }
            }
            @Override
            protected void onError(BadRequest error) {

            }
        });
    }

}