package com.richonmart.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.*;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.BaseFragment;
import com.richonmart.model.APIResponse;
import com.richonmart.model.AccountMenu;
import com.richonmart.model.User;
import com.richonmart.utils.Extension;

import java.util.ArrayList;

public class AccountFragment extends BaseFragment {

    @BindView(R.id.llActivate)
    LinearLayout llActivate;

    @BindView(R.id.llMarecoWallet)
    LinearLayout llMarecoWallet;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;

    @BindView(R.id.tvEmail)
    TextView tvEmail;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;


    private ArrayList<AccountMenu> menus = new ArrayList<>();

    @Override
    protected int getContentViewResource() {
        return R.layout.fragment_account;
    }

    @Override
    protected void onViewCreated() {
        setupProfile();
        swipeRefresh.setColorSchemeResources(R.color.primary);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                checkWalletStatus();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        checkWalletStatus();
    }

    private void checkWalletStatus() {
        if (MarecoAPI.isLoggedIn()) {
            setupBalance();
            getMarecoProfile();
            llActivate.setVisibility(View.GONE);
            llMarecoWallet.setVisibility(View.VISIBLE);
        }
        else {
            llActivate.setVisibility(View.VISIBLE);
            llMarecoWallet.setVisibility(View.GONE);
        }
    }

    private void getMarecoProfile() {
        MarecoAPI.service().getMarecoUserProfile().enqueue(new APICallback<APIResponse>(getContext(), 2) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                setupBalance();
                swipeRefresh.setRefreshing(false);
            }

            @Override
            protected void onError(BadRequest error) {
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void setupProfile() {
        User user = API.currentUser();
        tvEmail.setText(user.getEmail());
        tvName.setText(user.getFullname());
        String phoneNumber = user.getPhoneNumber();
        tvPhoneNumber.setText(phoneNumber);
    }

    private void setupBalance() {
        tvBalance.setText(Extension.priceFormat(MarecoAPI.getUser().getBalances().getPrimary()));
    }

    @OnClick(R.id.btnActivate)
    void activateClicked() {
        Intent intent = new Intent(getActivity(), ActivateMarecoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llMembership)
    void membershipClicked() {
        Intent intent = new Intent(getActivity(), MembershipActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llMarecoWallet)
    void walletClicked() {
        Intent intent = new Intent(getActivity(), MarecoAccountActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llSettings)
    void toSetting() {
        Intent intent = new Intent(getActivity(), AccountSettingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvEditProfile)
    void toAccountSetting() {
        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llLogout)
    void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.logout_desc))
                .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        API.setToken(null);
                        MarecoAPI.setUser(null);
                        MarecoAPI.setToken(null);
                        MarecoAPI.setLogInStatus(false);
                        dialog.dismiss();
                        callLogOutAPI();
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);;
                        if (getActivity() != null) {
                            getActivity().finish();
                        }
                    }
                }).setNegativeButton(getString(R.string.batal), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }

    private void callLogOutAPI() {

        API.service().logout().enqueue(new APICallback<APIResponse>(getContext()) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {

            }

            @Override
            protected void onError(BadRequest error) {

            }
        });
    }

}