package com.richonmart.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.gson.JsonElement;
import com.richonmart.R;
import com.richonmart.activity.ForgotPasswordActivity;
import com.richonmart.activity.MainActivity;
import com.richonmart.activity.RegisterActivity;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.BaseFragment;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Login;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

import java.util.Map;
import java.util.Set;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;


    @Override
    protected int getContentViewResource() {
        return R.layout.fragment_login;
    }

    @Override
    protected void onViewCreated() {

    }

    @OnClick(R.id.btnLogin)
    void login() {
        btnLogin.setEnabled(false);
        Extension.showLoading(getActivity());
        API.service().login(new Login(etEmail.getText().toString(), etPassword.getText().toString())).enqueue(new APICallback<APIResponse>(getContext()) {
            @Override
            protected void onSuccess(APIResponse response) {
                Extension.dismissLoading();
                API.setToken(response.getData().getToken());
                API.setUser(response.getData().getUser());
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                btnLogin.setEnabled(true);
            }

            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();
                btnLogin.setEnabled(true);

                if (error.code == 400) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle(getString(R.string.sorry));
                    alertDialog.setMessage(error.errorDetails);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    try {
                        Set<Map.Entry<String, JsonElement>> entries = error.errors.entrySet();//will return members of your object
                        for (Map.Entry<String, JsonElement> entry : entries) {
                            if (entry.getKey().matches("email")) {
                                etEmail.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("password")) {
                                etPassword.setError(entry.getValue().getAsString());
                            }
                        }

                    } catch (Exception exception) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle(getString(R.string.sorry));
                        alertDialog.setMessage(error.errorDetails);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }
            }
        });
    }

    @OnClick(R.id.btnRegister)
    void registerClicked() {
        Intent intent = new Intent(getActivity(), RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvForgotPassword)
    void forgotPasswordClicked() {
        Intent intent = new Intent(getActivity(), ForgotPasswordActivity.class);
        startActivity(intent);
    }

}