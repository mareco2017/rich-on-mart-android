package com.richonmart.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.CheckoutActivity;
import com.richonmart.adapter.HomeProductAdapter;
import com.richonmart.api.API;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.BaseFragment;
import com.richonmart.model.Product;
import com.richonmart.utils.Extension;

import java.util.ArrayList;
import java.util.List;

public class CartFragment extends BaseFragment {

    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;

    @BindView(R.id.tvSubtotal)
    TextView tvSubtotal;

    @BindView(R.id.llCheckout)
    LinearLayout llCheckout;

    private HomeProductAdapter mAdapter;

    private ArrayList<Product> promotionProducts = new ArrayList<>();

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_cart;
    }

    @Override
    protected void onViewCreated() {
        rvProduct.setLayoutManager(new LinearLayoutManager(getContext(), GridLayoutManager.VERTICAL, false));
        mAdapter = new HomeProductAdapter(getActivity(), 2, new HomeProductAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, final int position, Product product) {
                if (position != -1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Hapus produk ini dari cart Anda?")
                            .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    API.removeCartProduct(position);
                                    setupProduct();
                                    updateSubtotal();
                                }
                            }).setNegativeButton(getString(R.string.batal), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
                else {
                    updateSubtotal();
                }
            }
        });
        rvProduct.setAdapter(mAdapter);
    }

    private void updateSubtotal() {
        if (tvSubtotal == null) {
            return;
        }
        double total = 0;
        List<Product> products = API.getCartProduct();
        for (int l = 0; l < products.size(); l++) {
            Product product = products.get(l);
            double pricePerQty = product.getCalcutaltedPrice() * product.getCartQty();
            total += pricePerQty;
        }
        tvSubtotal.setText(Extension.priceFormat(total));
    }

    @OnClick(R.id.btnCheckout)
    void checkout() {
        if (API.isLoggedIn() && MarecoAPI.isLoggedIn()) {
            Intent intent = new Intent(getActivity(), CheckoutActivity.class);
            startActivity(intent);
        }
        else if (!API.isLoggedIn()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Silahkan Login terlebih dahulu")
                    .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        }
        else if (!MarecoAPI.isLoggedIn()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Silahkan Aktifkan Mareco Anda terlebih dahulu")
                    .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            builder.show();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        setupProduct();
        updateSubtotal();
    }

    private void setupProduct() {
        List<Product> products = API.getCartProduct();
        if (products.size() > 0) {
            if (mAdapter != null) {
                llCheckout.setVisibility(View.VISIBLE);
                mAdapter.setItems(products);
            }
        }
        else {
            if (llCheckout != null) {
                llCheckout.setVisibility(View.GONE);
                mAdapter.setItems(products);
            }
        }
    }
}