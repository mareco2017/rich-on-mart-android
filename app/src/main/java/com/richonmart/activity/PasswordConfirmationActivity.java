package com.richonmart.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.gson.JsonElement;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.User;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

import java.util.Map;
import java.util.Set;

public class PasswordConfirmationActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etPassword)
    EditText etPassword;
    User user;
    String type = "update_profile";

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_password_confirmation;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.perubahan_akun_profile));
        if (getIntent().getStringExtra("TYPE") != null) {
            type = getIntent().getStringExtra("TYPE");
        }
        if (type.equals("update_profile")) {
            user = (User) getIntent().getSerializableExtra("USER");
        }
        else {

        }
    }

    @OnClick(R.id.btnSave)
    void onSaveClicked() {
        if (type.equals("update_profile")) {
            updateProfile();
        }
        else {
            changePassword();
        }
    }

    private void changePassword() {
        runOnUiThread(new Runnable() {
            public void run() {
                Extension.showLoading(PasswordConfirmationActivity.this);
            }
        });
        MultipartBody.Builder buildernew = new MultipartBody.Builder();
        buildernew.setType(MultipartBody.FORM);
        String password = getIntent().getStringExtra("PASSWORD");
        buildernew.addFormDataPart("password", etPassword.getText().toString());
        buildernew.addFormDataPart("new_password", password);
        buildernew.addFormDataPart("new_password_confirmation", password);

        MultipartBody requestBody = buildernew.build();

        API.service().changePassword(requestBody).enqueue(new APICallback<APIResponse>(PasswordConfirmationActivity.this) {
            @Override
            protected void onSuccess(APIResponse response) {
                Extension.dismissLoading();
                AlertDialog alertDialog = new AlertDialog.Builder(PasswordConfirmationActivity.this).create();
                alertDialog.setTitle(getString(R.string.perubahan_berhasil));
                alertDialog.setMessage(getString(R.string.desc_update_password_success));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                onBackPressed();
                            }
                        });
                alertDialog.show();
            }

            @Override
            protected void onError(BadRequest error) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();

                    }
                });

                if (error.code == 400) {
                    AlertDialog alertDialog = new AlertDialog.Builder(PasswordConfirmationActivity.this).create();
                    alertDialog.setTitle(getString(R.string.sorry));
                    alertDialog.setMessage(error.errorDetails);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                }
            }
        });
    }

    private void updateProfile() {
        runOnUiThread(new Runnable() {
            public void run() {
                Extension.showLoading(PasswordConfirmationActivity.this);
            }
        });
        MultipartBody.Builder buildernew = new MultipartBody.Builder();
        buildernew.setType(MultipartBody.FORM);

        String nameArray[] = user.getFullname().split(" ", 2);
        buildernew.addFormDataPart("first_name", ((nameArray.length == 2) ? nameArray[0] : user.getFullname()));
        buildernew.addFormDataPart("last_name", ((nameArray.length == 2) ? nameArray[1] : ""));
        buildernew.addFormDataPart("phone_number", user.getPhoneNumber());
        buildernew.addFormDataPart("email", user.getEmail());
        buildernew.addFormDataPart("password", etPassword.getText().toString());

        MultipartBody requestBody = buildernew.build();

        API.service().updateProfile(requestBody).enqueue(new APICallback<APIResponse>(PasswordConfirmationActivity.this) {
            @Override
            protected void onSuccess(APIResponse response) {
                Extension.dismissLoading();
                AlertDialog alertDialog = new AlertDialog.Builder(PasswordConfirmationActivity.this).create();
                alertDialog.setTitle(getString(R.string.perubahan_berhasil));
                alertDialog.setMessage(getString(R.string.desc_update_profile_success));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                onBackPressed();
                            }
                        });
                alertDialog.show();
            }

            @Override
            protected void onError(BadRequest error) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();

                    }
                });

                if (error.code == 400) {
                    AlertDialog alertDialog = new AlertDialog.Builder(PasswordConfirmationActivity.this).create();
                    alertDialog.setTitle(getString(R.string.sorry));
                    alertDialog.setMessage(error.errorDetails);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    try {
                        Set<Map.Entry<String, JsonElement>> entries = error.errors.entrySet();//will return members of your object
                        for (Map.Entry<String, JsonElement> entry : entries) {
                            if (entry.getKey().matches("first_name") || entry.getKey().matches("last_name")) {
                                showError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("phone_number")) {
                                showError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("email")) {
                                showError(entry.getValue().getAsString());
                            }
                        }

                    } catch (Exception exception) {
                        Log.e("loginAPI", "" + exception);
                        AlertDialog alertDialog = new AlertDialog.Builder(PasswordConfirmationActivity.this).create();
                        alertDialog.setTitle(getString(R.string.sorry));
                        alertDialog.setMessage(error.errorDetails);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }
            }
        });
    }

    private void showError(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(PasswordConfirmationActivity.this).create();
        alertDialog.setTitle(getString(R.string.sorry));
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        onBackPressed();
                    }
                });
        alertDialog.show();
    }

}

