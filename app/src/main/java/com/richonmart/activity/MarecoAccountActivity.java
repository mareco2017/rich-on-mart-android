package com.richonmart.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.User;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

public class MarecoAccountActivity extends ToolbarActivity {

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private boolean isViewLoaded = false;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_mareco_account;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.mareco));
        User user = MarecoAPI.getUser();
        tvBalance.setText(Extension.numberPriceFormat(user.getBalances().getPrimary()));
        tvPhoneNumber.setText(user.getPhoneNumber());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @OnClick(R.id.llTopUp)
    void onTopUp() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("TYPE", "top_up");
        startActivity(intent);
    }

}