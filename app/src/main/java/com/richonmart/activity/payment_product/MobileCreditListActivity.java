package com.richonmart.activity.payment_product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.adapter.MobileCreditAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.PaymentProduct;

import java.util.List;

public class MobileCreditListActivity extends ToolbarActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private MobileCreditAdapter mAdapter;
    private String type;
    private String number;
    private int viewType;

    public static final int PRODUCT_RESULT = 1022;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_mobile_credit_selection;
    }

    @Override
    protected void onViewCreated() {
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            type = extra.getString("TYPE");
            viewType = extra.getInt("VIEWTYPE", 0);
            number = String.valueOf(extra.getString("NUMBER"));
        } else {
            type = null;
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MobileCreditAdapter(this, new MobileCreditAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position, PaymentProduct payment) {
                Intent intent = new Intent();
                intent.putExtra("product_id", payment.getId());
                intent.putExtra("product_name", payment.getTitle());
                intent.putExtra("total_price", payment.getFinalPrice());
                setResult(RESULT_OK, intent);
                finish();
            }
        }, viewType);
        recyclerView.setAdapter(mAdapter);
        getVouchers();
    }

    private void getVouchers() {
        API.service().getPaymentProductByType(type.equals("-1") ? "" : type, number.equals("") ? "" : "0" + number, type.equals("-1") ? "1" : "0").enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                List<PaymentProduct> payments = apiResponse.getData().getPayments();
                if (payments.size() > 0) {
                    mAdapter.setItems(payments);
                }
            }
            @Override
            protected void onError(BadRequest error) {

            }
        });
    }


}
