package com.richonmart.activity.payment_product;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.adapter.HistoryPaymentAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Order;

import java.util.List;

public class PaymentHistoryActivity extends ToolbarActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private HistoryPaymentAdapter mAdapter;


    @Override
    protected int getContentViewResource() {
        return R.layout.activity_mobile_credit_selection;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.history_transaction));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new HistoryPaymentAdapter(this, new HistoryPaymentAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position, Order order) {
                Intent intent = new Intent(PaymentHistoryActivity.this, HistoryDetailActivity.class);
                intent.putExtra("Order", order);
                intent.putExtra("TYPE", getIntent().getIntExtra("TYPE",11));
                startActivity(intent);
            }
        }, getIntent().getIntExtra("TYPE",11));
        recyclerView.setAdapter(mAdapter);
        getOrder();
    }

    private void getOrder() {
        API.service().getOrder(getIntent().getIntExtra("TYPE",11),0,50, "2,3").enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                List<Order> orders = apiResponse.getData().getOrders();
                if (orders.size() > 0) {
                    mAdapter.setItems(orders);
                }
            }
            @Override
            protected void onError(BadRequest error) {

            }
        });
    }

}
