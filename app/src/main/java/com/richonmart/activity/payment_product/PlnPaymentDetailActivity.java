package com.richonmart.activity.payment_product;

import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.Bill;
import com.richonmart.model.Inquiry;
import com.richonmart.utils.Extension;

import java.util.List;

public class PlnPaymentDetailActivity   extends ToolbarActivity {

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvAdmin)
    TextView tvAdmin;

    @BindView(R.id.tvTotalDenda)
    TextView tvTotalDenda;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;


    private List<Bill> bills;

    private Inquiry inquiry;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_pln_payment_detail;
    }

    @Override
    protected void onViewCreated() {
        inquiry = (Inquiry) getIntent().getSerializableExtra("INQUIRY");
        bills = inquiry.getBillDetail();
        tvTotalBill.setText(inquiry.getJumlahTagihan());
        if (inquiry.getBillDetail().size() > 0) {
            Bill bill = bills.get(0);
            tvTotalDenda.setText(Extension.priceFormat(bill.getDenda()));
            double admin = 0;
            double total = 0;
            for(int l=0; l<inquiry.getBillDetail().size(); l++){
                admin += inquiry.getBillDetail().get(l).getAdmin();
                total += inquiry.getBillDetail().get(l).getNilaiTagihan();
            }
            tvTotalBill.setText(Extension.priceFormat(total));
            tvAdmin.setText(Extension.priceFormat(admin));
            tvTotalPrice.setText(Extension.priceFormat(inquiry.getTotalTagihan()));
//            tvPeriode.setText(periode);
        }


    }

}
