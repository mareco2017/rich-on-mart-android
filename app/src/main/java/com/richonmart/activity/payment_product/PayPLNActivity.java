package com.richonmart.activity.payment_product;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.WebViewActivity;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.*;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PayPLNActivity extends ToolbarActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.llWallet)
    LinearLayout llWallet;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.tvTarif)
    TextView tvTarif;

    @BindView(R.id.tvStandMeter)
    TextView tvStandMeter;

    @BindView(R.id.tvCustomerName)
    TextView tvCustomerName;

    @BindView(R.id.tvNomorMeter)
    TextView tvNomorMeter;

    @BindView(R.id.llInformation)
    LinearLayout llInformation;

    @BindView(R.id.etCustomerNumber)
    EditText etCustomerNumber;

    @BindView(R.id.radioTagihan)
    RadioButton radioTagihan;

    @BindView(R.id.radioToken)
    RadioButton radioToken;

    @BindView(R.id.llBill)
    LinearLayout llBill;

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvPeriode)
    TextView tvPeriode;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;

    @BindView(R.id.tvTotalPayment)
    TextView tvTotalPayment;

    @BindView(R.id.tvIdpel)
    TextView tvIdpel;

    @BindView(R.id.btnPay)
    Button btnPay;

    int type;

    Bill bill;

    Inquiry inquiry;

    int id = -1;

    private Timer timer;

    private PaymentProduct paymentTagihan;

    public static boolean isPaymentSuccess = false;

    String customerNumber;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_pay_pln;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.pln));
        Extension.setupWallet(llWallet, tvBalance, this);
        type = getIntent().getIntExtra("TYPE", 0);

        etCustomerNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
                customerNumber = s.toString();
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (TextUtils.isEmpty(s.toString().trim())) {
//                            requestInquiry();
                        } else {
                            requestInquiry();
                        }
                        // do your actual work here
                    }
                }, 600);
            }
        });
        getIdByType("5");
        radioTagihan.setChecked(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaymentSuccess) {
            resetBill();
        }
    }

    @OnClick(R.id.btnPay)
    void payBtnClicked() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("PRICE", inquiry.getTotalTagihan());
        intent.putExtra("DESC", etCustomerNumber.getText().toString() + " " + inquiry.getName());
        intent.putExtra("TYPE", "5");
        intent.putExtra("ID", id);
        startActivity(intent);
    }

    private void getIdByType(String type) {
        API.service().getPaymentProductByType(type, "", "1").enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                List<PaymentProduct> payments = apiResponse.getData().getPayments();
                if (payments.size() > 0) {
                    paymentTagihan = apiResponse.getData().getPayments().get(0);
                }
            }
            @Override
            protected void onError(BadRequest error) {

            }
        });
    }

    private void requestInquiry() {
        String plnType = "";
        if (radioTagihan.isChecked()) {
            if (paymentTagihan != null) {
                plnType = String.valueOf(paymentTagihan.getId());
                requestInquiry(plnType);
            }
        }
    }

    private void requestInquiry(String type) {
        MultipartBody.Builder buildernew = new MultipartBody.Builder();
        buildernew.setType(MultipartBody.FORM);

        buildernew.addFormDataPart("id", type);
        buildernew.addFormDataPart("customer_id", etCustomerNumber.getText().toString());
        MultipartBody requestBody = buildernew.build();

        API.service().plnInquiry(requestBody).enqueue(new APICallback<APIResponse>(PayPLNActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                id = apiResponse.getData().getOrder().getId();
                inquiry = apiResponse.getData().getOrder().getOptions().getInquiry();
                PayPLNActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        setupView(apiResponse.getData().getOrder());
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                PayPLNActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(PayPLNActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public void resetBill() {
        llBill.setVisibility(View.GONE);
        etCustomerNumber.setText("");
        llInformation.setVisibility(View.GONE);
        btnPay.setEnabled(false);
        btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_dark_gray);
    }

    @OnClick(R.id.tvDetail)
    void detailClicked() {
        Intent intent = new Intent(this, PlnPaymentDetailActivity.class);
        intent.putExtra("INQUIRY", inquiry);
//        intent.putExtra("BILL", bill);
        startActivity(intent);
    }

    private  void setupView(Order order) {
        btnPay.setEnabled(true);
        btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_orange);
        llInformation.setVisibility(View.VISIBLE);
        llBill.setVisibility(View.VISIBLE);
        Inquiry inquiry = order.getOptions().getInquiry();
        tvNomorMeter.setText(etCustomerNumber.getText().toString());
        tvTarif.setText(inquiry.getTarif() + "/" + inquiry.getDaya() + " VA");
        tvIdpel.setText(inquiry.getSubscriberID());
        tvTotalBill.setText(String.valueOf(inquiry.getLembarTagihan()));
        tvCustomerName.setText(inquiry.getName());
        if (inquiry.getBillDetail().size() > 0) {
            Bill bill = inquiry.getBillDetail().get(0);
            this.bill = bill;
            String periode = "";
            for(int l=0; l<inquiry.getBillDetail().size(); l++){
                if (l == 0) {
                    periode = inquiry.getBillDetail().get(l).getPeriode();
                }
                else {
                    periode = periode + ", " + inquiry.getBillDetail().get(l).getPeriode();
                }
            }
            tvPeriode.setText(periode);
            tvTotalPrice.setText(Extension.numberPriceFormat(inquiry.getTotalTagihan()));
        }
        tvTotalPayment.setText(Extension.priceFormat(inquiry.getTotalTagihan()));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.history, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history: {
                Intent intent = new Intent(PayPLNActivity.this, PaymentHistoryActivity.class);
                intent.putExtra("TYPE", 17);
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
