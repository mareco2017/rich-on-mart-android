package com.richonmart.activity.payment_product;

import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;

public class TelkomActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_payment_telkom;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.pascabayar));
    }

}
