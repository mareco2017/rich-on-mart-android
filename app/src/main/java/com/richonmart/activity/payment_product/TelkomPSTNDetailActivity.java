package com.richonmart.activity.payment_product;

import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.Bill;
import com.richonmart.model.Inquiry;
import com.richonmart.utils.Extension;

import java.util.List;

public class TelkomPSTNDetailActivity  extends ToolbarActivity {

    @BindView(R.id.tvBill)
    TextView tvBill;

    @BindView(R.id.tvPeriode)
    TextView tvPeriode;

    @BindView(R.id.tvAdmin)
    TextView tvAdmin;

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;


    private List<Bill> bills;

    private Inquiry inquiry;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_payment_telkom_detail;
    }

    @Override
    protected void onViewCreated() {
        inquiry = (Inquiry) getIntent().getSerializableExtra("INQUIRY");
        bills = inquiry.getBill();
        tvTotalBill.setText(inquiry.getJumlahTagihan());
        if (inquiry.getBill().size() > 0) {
            Bill bill = bills.get(0);
            tvAdmin.setText(Extension.priceFormat(bill.getAdmin()));
            tvTotalPrice.setText(Extension.priceFormat(bill.getTotal()));
            String periode = "";
            for(int l=0; l<bills.size(); l++){
                if (l == 0) {
                    periode = bills.get(l).getPeriode();
                }
                else {
                    periode = periode + ", " + bills.get(l).getPeriode();
                }
            }
            tvPeriode.setText(periode);
            tvBill.setText(Extension.priceFormat(bill.getNilaiTagihan()));
        }


    }

}
