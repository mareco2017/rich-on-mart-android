package com.richonmart.activity.payment_product;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.WebViewActivity;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.utils.Extension;


public class PhoneAndDataActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;

    @BindView(R.id.etAmount)
    EditText etAmount;

    @BindView(R.id.tvTotalPayment)
    TextView tvTotalPayment;

    @BindView(R.id.btnPay)
    Button btnPay;

    @BindView(R.id.llWallet)
    LinearLayout llWallet;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    double amount;

    int id;

    int type;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_phone_credit;
    }

    @Override
    protected void onViewCreated() {
        type = getIntent().getIntExtra("TYPE", 1);
        if (type == 1) {
            tvTitle.setText(getString(R.string.pulsa));
        }
        else {
            tvTitle.setText(getString(R.string.paket_data));
        }
        Extension.setupWallet(llWallet,tvBalance, this);
    }

    @OnClick(R.id.llDropdown)
    void toSelectProducts() {
        if (etPhoneNumber.getText().toString().length() > 0) {
            Intent intent = new Intent(this, MobileCreditListActivity.class);
            intent.putExtra("TYPE", String.valueOf(type));
            intent.putExtra("VIEWTYPE", 1);
            intent.putExtra("NUMBER", etPhoneNumber.getText().toString());
            startActivityForResult(intent, 1);
        }
        else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setMessage("Silahkan masukkin nomor handphone Anda terlebih dahulu");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    @OnClick(R.id.btnPay)
    void payBtnClicked() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("PRICE", amount);
        intent.putExtra("DESC", etAmount.getText().toString());
        intent.putExtra("TYPE",  type == 0 ? "paket" : "pulsa" );
        intent.putExtra("ID", id);
        intent.putExtra("PHONE_NUMBER", "0"+etPhoneNumber.getText().toString());
        startActivity(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                id = data.getIntExtra("product_id", 0);
                btnPay.setEnabled(true);
                btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_orange);
                etAmount.setText(data.getStringExtra("product_name"));
                amount = data.getDoubleExtra("total_price",0);
                tvTotalPayment.setText(Extension.numberPriceFormat(data.getDoubleExtra("total_price",0)));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.history, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history: {
                Intent intent = new Intent(PhoneAndDataActivity.this, PaymentHistoryActivity.class);
                intent.putExtra("TYPE", 19);
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}