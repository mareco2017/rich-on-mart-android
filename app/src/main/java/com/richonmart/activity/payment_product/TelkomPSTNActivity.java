package com.richonmart.activity.payment_product;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.WaterBillActivity;
import com.richonmart.activity.WaterBillDetailActivity;
import com.richonmart.activity.WebViewActivity;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.*;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

import java.util.Timer;
import java.util.TimerTask;

public class TelkomPSTNActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.etCustomerNumber)
    EditText etCustomerNumber;

    @BindView(R.id.llWallet)
    LinearLayout llWallet;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.tvCustomerNumber)
    TextView tvCustomerNumber;


    @BindView(R.id.tvCustomerName)
    TextView tvCustomerName;

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvPeriode)
    TextView tvPeriode;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;

    @BindView(R.id.tvTotalPayment)
    TextView tvTotalPayment;

    @BindView(R.id.llInformation)
    LinearLayout llInformation;

    @BindView(R.id.llBill)
    LinearLayout llBill;

    @BindView(R.id.btnPay)
    Button btnPay;

    String customerNumber;

    private Timer timer;

    Bill bill;

    Inquiry inquiry;

    int id;

    public static boolean isPaymentSuccess = false;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_payment_telkom;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.telepon));
        Extension.setupWallet(llWallet,tvBalance, this);
        etCustomerNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
                customerNumber = s.toString();
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // do your actual work here
                        if (TextUtils.isEmpty(s.toString().trim())) {
                            requestInquiry();
                        } else {
                            requestInquiry();
                        }
                    }
                }, 600);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaymentSuccess) {
            resetBill();
        }
    }

    public void resetBill() {
        llBill.setVisibility(View.GONE);
        etCustomerNumber.setText("");
        llInformation.setVisibility(View.GONE);
        btnPay.setEnabled(false);
        btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_dark_gray);
    }

    private void requestInquiry() {
        MultipartBody.Builder buildernew = new MultipartBody.Builder();
        buildernew.setType(MultipartBody.FORM);

        buildernew.addFormDataPart("id", "2");
        buildernew.addFormDataPart("customer_id", etCustomerNumber.getText().toString());
        MultipartBody requestBody = buildernew.build();

        API.service().telkomInquiry(requestBody).enqueue(new APICallback<APIResponse>(TelkomPSTNActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                id = apiResponse.getData().getOrder().getId();
                inquiry = apiResponse.getData().getOrder().getOptions().getInquiry();
                TelkomPSTNActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        setupView(apiResponse.getData().getOrder());
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                TelkomPSTNActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(TelkomPSTNActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @OnClick(R.id.tvDetail)
    void detailClicked() {
        Intent intent = new Intent(this, TelkomPSTNDetailActivity.class);
        intent.putExtra("BILL", bill);
        startActivity(intent);
    }

    private void setupView(Order order) {
        llBill.setVisibility(View.VISIBLE);
        llInformation.setVisibility(View.VISIBLE);
        tvCustomerNumber.setText(customerNumber);
        btnPay.setEnabled(true);
        btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_orange);

        Option options = order.getOptions();
        Inquiry inquiry = options.getInquiry();
        tvTotalBill.setText(inquiry.getJumlahTagihan());
        tvCustomerName.setText(inquiry.getName());
        if (inquiry.getBill().size() > 0) {
            Bill bill = inquiry.getBill().get(0);
            this.bill = bill;
            String periode = "";
            for(int l=0; l<inquiry.getBill().size(); l++){
                if (l == 0) {
                    periode = inquiry.getBill().get(l).getPeriode();
                }
                else {
                    periode = periode + ", " + inquiry.getBill().get(l).getPeriode();
                }
            }
            tvPeriode.setText(periode);
            tvTotalPrice.setText(Extension.numberPriceFormat(bill.getTotal()));
        }
        tvTotalPayment.setText(Extension.priceFormat(inquiry.getTotalTagihan()));
    }

    @OnClick(R.id.btnPay)
    void payBtnClicked() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("PRICE", bill.getTotal());
        intent.putExtra("DESC", "TELKOM PSTN" + inquiry.getIdpel());
        intent.putExtra("TYPE", "telkom");
        intent.putExtra("ID", id);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.history, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history: {
                Intent intent = new Intent(TelkomPSTNActivity.this, PaymentHistoryActivity.class);
                intent.putExtra("TYPE", 12);
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
