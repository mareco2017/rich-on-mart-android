package com.richonmart.activity.payment_product;

import android.content.Intent;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.WaterBillDetailActivity;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.Bill;
import com.richonmart.model.Inquiry;
import com.richonmart.model.Order;
import com.richonmart.utils.Extension;

public class HistoryDetailActivity extends ToolbarActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvInvoiceNo)
    TextView tvInvoiceNo;

    @BindView(R.id.tvAmount)
    TextView tvAmount;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvType)
    TextView tvType;

    @BindView(R.id.tvTotalPay)
    TextView tvTotalPay;

    @BindView(R.id.tvBill)
    TextView tvBill;

    @BindView(R.id.tvPeriode)
    TextView tvPeriode;

    @BindView(R.id.tvDetail)
    TextView tvDetail;

    @BindView(R.id.ivCategory)
    ImageView ivCategory;

    @BindView(R.id.tvCategory)
    TextView tvCategory;

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.llBill)
    LinearLayout llBill;

    private Order order;

    private int type;


    @Override
    protected int getContentViewResource() {
        return R.layout.activity_history_payment;
    }

    @Override
    protected void onViewCreated() {
        type = getIntent().getIntExtra("TYPE", 11);
        order = (Order) getIntent().getSerializableExtra("Order");
        tvTitle.setText(order.getReferenceNumber());
        tvInvoiceNo.setText(order.getReferenceNumber());
        tvAmount.setText(Extension.priceFormat(order.getTotal()));
        tvTotalPay.setText(Extension.priceFormat(order.getTotal()));
        tvDate.setText(Extension.receiptDetailDateFormat.format(order.getCreatedAt()));
        if (type == 11) {
            tvType.setText(R.string.air_atb);
            setupBill();
        } else if (type == 12) {
            tvDetail.setVisibility(View.GONE);
            tvCategory.setText(order.getOptions().getInquiry().getProductCode());
            tvType.setText(order.getOptions().getInquiry().getProductCode());
            ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pascabayar));
            setupBill();
        }
        else if (type == 17) {
            llBill.setVisibility(View.GONE);
            tvCategory.setText(R.string.tagihan_listrik);
            tvType.setText(order.getOptions().getInquiry().getProductCode());
            ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_pln));
        }
        else if (type == 15) {
            tvDetail.setVisibility(View.GONE);
            tvType.setText(order.getOptions().getInquiry().getProductCode());
            tvCategory.setText(R.string.tagihan_listrik);
            ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.telepon));
            setupBill();
        } else if (type == 16) {
            tvDetail.setVisibility(View.GONE);
            tvType.setText(order.getOptions().getInquiry().getProductCode());
            tvCategory.setText(order.getOptions().getInquiry().getProductCode());
            ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.internet));
            setupBill();
        } else {
            llBill.setVisibility(View.GONE);
            ivCategory.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_phone_credit));
            if (order.getOrderDetail().size() > 0) {
                tvCategory.setText(order.getOrderDetail().get(0).getProductable().getTitle());
                tvType.setText(order.getOrderDetail().get(0).getProductable().getTitle());
            }
        }
    }

    private void setupBill() {
        if (order.getOptions().getInquiry().getBill().size() > 0) {
            Bill bill = order.getOptions().getInquiry().getBill().get(0);
            Inquiry inquiry = order.getOptions().getInquiry();
            String periode = "";
            for (int l = 0; l < inquiry.getBill().size(); l++) {
                if (l == 0) {
                    periode = inquiry.getBill().get(l).getPeriode();
                } else {
                    periode = periode + ", " + inquiry.getBill().get(l).getPeriode();
                }
            }
            if (order.getStatus() ==  2) {
                tvStatus.setTextColor(ContextCompat.getColor(this, R.color.yellow));
                tvStatus.setText(getString(R.string.pending));
            }
            tvBill.setText(inquiry.getJumlahTagihan());
            tvTotalBill.setText(Extension.priceFormat(order.getTotal()));
            tvPeriode.setText(periode);
        }
    }

    @OnClick(R.id.tvDetail)
    void detailClicked() {
        if (order.getOptions().getInquiry().getBill().size() > 0) {
            Bill bill = order.getOptions().getInquiry().getBill().get(0);
            Intent intent = new Intent(this, WaterBillDetailActivity.class);
            intent.putExtra("BILL", bill);
            intent.putExtra("ORDER", order);
            startActivity(intent);
        }
    }
}
