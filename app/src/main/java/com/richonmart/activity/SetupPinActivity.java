package com.richonmart.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.PIN;
import com.richonmart.utils.Extension;

public class SetupPinActivity extends ToolbarActivity {

    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    @BindView(R.id.tvDescription)
    TextView tvDescription;


    private String password = "";
    private String previousPin = "";
    private String type = "set_pin";

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_setup_pin;
    }

    @Override
    protected void onViewCreated() {

        if (getIntent().getStringExtra("TYPE") != null) {
            type = getIntent().getStringExtra("TYPE");
        }
        if (getIntent().getStringExtra("PIN") != null) {
            previousPin = getIntent().getStringExtra("PIN");
        }
        if (type.equals("verify_pin")) {
            tvDescription.setText(getString(R.string.verify_pin));
        }
    }


    void submitPin() {
        if (type.equals("set_pin")) {
            Intent intent = new Intent(SetupPinActivity.this, SetupPinActivity.class);
            intent.putExtra("TYPE", "verify_pin");
            intent.putExtra("PIN", password);
            startActivity(intent);
            finish();
        }
        else {
            if (!password.equals(previousPin)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SetupPinActivity.this);
                builder.setTitle(getString(R.string.sorry))
                        .setMessage("Maaf Pin tidak sesuai")
                        .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                builder.show();
            }
            else {
                setPin();
            }
        }
    }

    void setPin() {
        Extension.showLoading(this);
        MarecoAPI.service().setPIN(new PIN(password,previousPin)).enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                initializeWallet();
            }

            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();
                if (error.errorDetails != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SetupPinActivity.this);
                    builder.setTitle(getString(R.string.sorry))
                            .setMessage(error.errorDetails)
                            .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    builder.show();
                }
            }
        });
    }

    private void initializeWallet() {
        MarecoAPI.service().initializeWallet().enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                Extension.dismissLoading();
                MarecoAPI.setUser(apiResponse.getData().getUser());
                MarecoAPI.setLogInStatus(true);
                Intent intent = new Intent(SetupPinActivity.this, MainMarecoActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();
                if (error.errorDetails != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SetupPinActivity.this);
                    builder.setTitle(getString(R.string.sorry))
                            .setMessage(error.errorDetails)
                            .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    builder.show();
                }
            }
        });
    }


    @OnClick({R.id.tvOne, R.id.tvTwo, R.id.tvThree, R.id.tvFour, R.id.tvFive, R.id.tvSix, R.id.tvSeven, R.id.tvEight, R.id.tvNine, R.id.tvZero, R.id.tvBackspace})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvOne: {
                if (password.length() < 6) {
                    password += "1";
                    setImages();
                }
                break;
            }
            case R.id.tvTwo: {
                if (password.length() < 6) {
                    password += "2";
                    setImages();
                }
                break;
            }
            case R.id.tvThree: {
                if (password.length() < 6) {
                    password += "3";
                    setImages();
                }
                break;
            }
            case R.id.tvFour: {
                if (password.length() < 6) {
                    password += "4";
                    setImages();
                }
                break;
            }
            case R.id.tvFive: {
                if (password.length() < 6) {
                    password += "5";
                    setImages();
                }
                break;
            }
            case R.id.tvSix: {
                if (password.length() < 6) {
                    password += "6";
                    setImages();
                }
                break;
            }
            case R.id.tvSeven: {
                if (password.length() < 6) {
                    password += "7";
                    setImages();
                }
                break;
            }
            case R.id.tvEight: {
                if (password.length() < 6) {
                    password += "8";
                    setImages();
                }
                break;
            }
            case R.id.tvNine: {
                if (password.length() < 6) {
                    password += "9";
                    setImages();
                }
                break;
            }
            case R.id.tvZero: {
                if (password.length() < 6) {
                    password += "0";
                    setImages();
                }
                break;
            }
            case R.id.tvBackspace: {
                if (password.length() > 0) {
                    password = password.substring(0, password.length() - 1);
                    setImages();
                }
                break;
            }
        }
    }

    private void setImages() {
        ratingBar.setRating(password.length());
        if (password.length() == 6) {
            submitPin();
        }
    }
}