package com.richonmart.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.orhanobut.hawk.Hawk;
import com.richonmart.R;
import com.richonmart.base.BaseActivity;

import java.util.List;
import java.util.Locale;

public class SplashActivity extends BaseActivity {

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onViewCreated() {
        new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(1000);
                } catch (Exception e) {
                    Log.e("ERROR", e.getMessage());
                } finally {
                    next();
                }
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void next() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

}
