package com.richonmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.gson.JsonElement;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.ForgotPassword;
import com.richonmart.model.Login;
import com.richonmart.utils.Extension;

import java.util.Map;
import java.util.Set;

public class ForgotPasswordActivity extends ToolbarActivity {


    @BindView(R.id.etEmail)
    TextInputEditText etEmail;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected void onViewCreated() {

    }

    @OnClick(R.id.btnSend)
    void login() {
        Extension.showLoading(ForgotPasswordActivity.this);
        API.service().forgotPassword(new ForgotPassword(etEmail.getText().toString())).enqueue(new APICallback<APIResponse>(ForgotPasswordActivity.this) {
            @Override
            protected void onSuccess(APIResponse response) {
                Extension.dismissLoading();
                final AlertDialog alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this).create();
//                alertDialog.setTitle(getString(R.string.sukses));
                alertDialog.setTitle(response.getMessage());
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });
                alertDialog.show();
            }

            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();

                if (error.code == 400) {
                    final AlertDialog alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this).create();
                    alertDialog.setTitle(getString(R.string.sorry));
                    alertDialog.setMessage(error.errorDetails);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    try {
                        Set<Map.Entry<String, JsonElement>> entries = error.errors.entrySet();//will return members of your object
                        for (Map.Entry<String, JsonElement> entry : entries) {
                            if (entry.getKey().matches("email")) {
                                etEmail.setError(entry.getValue().getAsString());
                            }
                        }

                    } catch (Exception exception) {
                        AlertDialog alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this).create();
                        alertDialog.setTitle(getString(R.string.sorry));
                        alertDialog.setMessage(error.errorDetails);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }
            }
        });
    }

}
