package com.richonmart.activity;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.utils.JavaScriptInterface;

public class MembershipActivity  extends ToolbarActivity {

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.tvTitle)
    TextView tvTittle;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.ivPackage)
    ImageView ivPackage;

    private String type = "pay";
    public int iuranId = 0;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_membership;
    }

    @Override
    protected void onViewCreated() {
        tvTittle.setText(R.string.membership);
        tvName.setText(API.currentUser().getFullname());
        String url = "";
        JavaScriptInterface jsInterface;

        url = "https://mart.richonpay.com/webview/package-info" ;
        int packge = API.currentUser().getPackages();
        if (packge == 0) {
//            ivPackage.setVisibility(View.GONE);
            ivPackage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.silver));
        }
        else if (packge == 1) {
            ivPackage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.silver));
        }
        else if (packge == 2 ) {
            ivPackage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.gold));
        }
        else if (packge == 3 ) {
            ivPackage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.platinum));
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
//                return super.shouldOverrideUrlLoading(view, request);
                view.loadUrl(request);
                return true;
            }
        });

    }
}
