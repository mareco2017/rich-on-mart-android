package com.richonmart.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.MarecoLogin;
import com.richonmart.model.User;
import com.richonmart.utils.Extension;

public class  OTPVerificationActivity extends ToolbarActivity {

    @BindView(R.id.etInput)
    EditText etInput;

    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;

    @BindView(R.id.tvOtpCount)
    TextView tvOtpCount;

    @BindView(R.id.tvCountdown)
    TextView tvCountdown;

    private CountDownTimer resendCountdown = new CountDownTimer(25000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            tvOtpCount.setEnabled(false);
            String countdownText = " ("+ String.valueOf((millisUntilFinished / 1000) + ")");
            tvOtpCount.setText(countdownText);
        }

        @Override
        public void onFinish() {
            tvCountdown.setText("");
            tvOtpCount.setEnabled(true);
            tvOtpCount.setText(getString(R.string.ulang_kirim_kode));
        }
    };

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_verify_otp;
    }

    @Override
    protected void onViewCreated() {
//        onResend();
        tvCountdown.setText(getString(R.string.kirim_ulang_dalam));
        resendCountdown.start();
        final TextWatcher txwatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 4) {
                    submit();
                }
//                sms_count.setText(String.valueOf(s.length()));
            }

            public void afterTextChanged(Editable s) {
            }
        };

        etInput.addTextChangedListener(txwatcher);
        User user = API.currentUser();
        tvPhoneNumber.setText(user.getPhoneNumber());
    }

    @OnClick(R.id.tvOtpCount)
    void onResend() {
        tvCountdown.setText(getString(R.string.kirim_ulang_dalam));
        resendCountdown.start();
        requestOtp();
    }


    void requestOtp() {
        User user = API.currentUser();
        MarecoAPI.service().requestOTP(new MarecoLogin( user.getPhoneNumber(),"")).enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {

            }

            @Override
            protected void onError(BadRequest error) {

            }
        });
    }

    void submit() {
        User user = API.currentUser();
        Extension.showLoading(this);
        MarecoAPI.service().loginMareco(new MarecoLogin(user.getPhoneNumber(),etInput.getText().toString())).enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                MarecoAPI.setToken(apiResponse.getData().getToken());
                MarecoAPI.setUser(apiResponse.getData().getUser());
                MarecoAPI.setLogInStatus(true);
                if (apiResponse.getData().getUser().isPinSet()) {
                    Extension.dismissLoading();
                    finish();
                }
                if (apiResponse.getData().getUser().getWallets() == null ) {
                    initializeWallets();
                }
                else {
                    Extension.dismissLoading();
                    Intent intent = new Intent(OTPVerificationActivity.this, SetupPinActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();
                if (error.errorDetails != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OTPVerificationActivity.this);
                    builder.setTitle(getString(R.string.sorry))
                            .setMessage(error.errorDetails)
                            .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    builder.show();
                }
            }
        });
    }

    private void initializeWallets() {
        MarecoAPI.service().initializeWallet().enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                Extension.dismissLoading();
                MarecoAPI.setUser(apiResponse.getData().getUser());
                finish();
            }

            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();
                if (error.errorDetails != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OTPVerificationActivity.this);
                    builder.setTitle(getString(R.string.sorry))
                            .setMessage(error.errorDetails)
                            .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                    builder.show();
                }
            }
        });
    }
}
