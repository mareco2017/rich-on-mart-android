package com.richonmart.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.payment_product.MobileCreditListActivity;
import com.richonmart.adapter.HomeProductAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Address;
import com.richonmart.model.Checkout;
import com.richonmart.model.Product;
import com.richonmart.utils.Extension;

import java.util.ArrayList;
import java.util.List;

public class CheckoutActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;

    @BindView(R.id.tvAddress)
    TextView tvAddress;

    @BindView(R.id.tvDeliverPrice)
    TextView tvDeliverPrice;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;

    @BindView(R.id.tvProductPrice)
    TextView tvProductPrice;

    @BindView(R.id.btnCheckout)
    Button btnCheckout;

    @BindView(R.id.radioInstant)
    RadioButton radioInstant;

    @BindView(R.id.radioRegular)
    RadioButton radioRegular;

    @BindView(R.id.rgType)
    RadioGroup rgType;

    private HomeProductAdapter mAdapter;

    private ArrayList<Product> promotionProducts = new ArrayList<>();

    private Address selectedAddress;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_checkout;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(R.string.checkout);
        rvProduct.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this, GridLayoutManager.VERTICAL, false));
        mAdapter = new HomeProductAdapter(CheckoutActivity.this, 2, new HomeProductAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, final int position, Product product) {
                if (position != -1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CheckoutActivity.this);
                    builder.setTitle("Hapus produk ini dari cart Anda?")
                            .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    API.removeCartProduct(position);
                                    setupProduct();
                                    updateSubtotal();
                                }
                            }).setNegativeButton(getString(R.string.batal), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                } else {
                    updateSubtotal();
                }
            }
        });
        rvProduct.setAdapter(mAdapter);
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                setupDeliveryPrice();
            }
        });
        radioInstant.setChecked(true);
        setupProduct();
        getAddress();
    }

    private void setupDeliveryPrice() {
        tvDeliverPrice.setText(Extension.priceFormat(getDeliveryPrice()));
        setupTotalPrice();
    }

    private double getDeliveryPrice() {
        if (radioInstant.isChecked()) {
            return 15000;
        } else {
            return 10000;
        }
    }

    private void setupTotalPrice() {
        double price = getDeliveryPrice() + getProductPrice();
        tvTotalPrice.setText(Extension.priceFormat(price));
    }

    private void getAddress() {
        API.service().getAddress().enqueue(new APICallback<APIResponse>(CheckoutActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                CheckoutActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        List<Address> addresses = apiResponse.getData().getAddresses();
                        for (int l = 0; l < addresses.size(); l++) {
                            if (addresses.get(l).isPrimary() == 1) {
                                selectedAddress = addresses.get(l);
                                setupAddress(selectedAddress);
                            }

                        }
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                CheckoutActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(CheckoutActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void setupAddress(Address address) {
        String fullAddress = "(" + address.getName() + ") " + address.getFullAddress();
        tvAddress.setText(fullAddress);
    }

    private void updateSubtotal() {
        tvProductPrice.setText(Extension.priceFormat(getProductPrice()));
        setupTotalPrice();
    }

    private double getProductPrice() {
        double total = 0;
        List<Product> products = API.getCartProduct();
        for (int l = 0; l < products.size(); l++) {
            Product product = products.get(l);
            double pricePerQty = product.getCalcutaltedPrice() * product.getCartQty();
            total += pricePerQty;
        }
        return total;
    }

    private void setupProduct() {
        List<Product> products = API.getCartProduct();
        if (products.size() > 0) {
            if (mAdapter != null) {
                mAdapter.setItems(products);
            }
        }
    }

    @OnClick(R.id.llAddress)
    void addressClicked() {
        Intent intent = new Intent(this, AddressListActivity.class);
        intent.putExtra("FROMCART", true);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.btnCheckout)
    void checkoutClicked() {
        Extension.showLoading(this);
        Checkout checkout = new Checkout();
        List<Product> products = API.getCartProduct();
        for (int l = 0; l < products.size(); l++) {
            Product product = products.get(l);
            product.setProductId(product.getId());
        }
        checkout.setAddressId(selectedAddress.getId());
        checkout.setDeliveryType(radioInstant.isChecked() ? 0 : 1);
        checkout.setProducts(products);

        API.service().checkout(checkout).enqueue(new APICallback<APIResponse>(CheckoutActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                CheckoutActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                        Intent intent = new Intent(CheckoutActivity.this, WebViewActivity.class);
                        intent.putExtra("PRICE", getDeliveryPrice() + getProductPrice());
                        intent.putExtra("DESC", "Rich on Mart");
                        intent.putExtra("TYPE", "rom");
                        intent.putExtra("ID", apiResponse.getData().getOrder().getId());
                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                CheckoutActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                        Toast.makeText(CheckoutActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                selectedAddress = (Address) data.getSerializableExtra("ADDRESS");
                setupAddress(selectedAddress);
            }
        }
    }

}
