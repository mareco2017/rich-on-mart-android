package com.richonmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.User;
import com.richonmart.utils.Extension;

public class EditProfileActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etUsername)
    EditText etUsername;

    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;


    @Override
    protected int getContentViewResource() {
        return R.layout.activity_edit_profile;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.edit_profil));
        setupProfile();
    }

    private void setupProfile() {
        User user = API.currentUser();
        etEmail.setText(user.getEmail());
        etPhoneNumber.setText(user.getPhoneNumber().replace("+62", ""));
        etUsername.setText(user.getFullname());
    }

    @OnClick(R.id.btnSave)
    void onSaveClicked() {
        try {
            if (etUsername.getText().toString().length() < 3) {
                showError(getString(R.string.please_insert_valid_name));
                return;
            }

            if (!Extension.isValidEmail(etEmail.getText().toString())) {
                showError(getString(R.string.please_insert_valid_email));
                return;
            }

            if (etPhoneNumber.getText().toString().length() < 8) {
                showError(getString(R.string.please_insert_valid_phone));
                return;
            }
            submitUpdate();
        } catch (Exception exception) {

        }
    }

    private void submitUpdate() {
        User user = new User();
        user.setFullname(etUsername.getText().toString());
        user.setEmail(etEmail.getText().toString());
        user.setPhoneNumber("+62" + etPhoneNumber.getText().toString());

        Intent intent = new Intent(this, PasswordConfirmationActivity.class);
        intent.putExtra("USER", user);
        startActivity(intent);
    }

    private void showError(String text) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(text);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
