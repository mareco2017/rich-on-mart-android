package com.richonmart.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.activity.payment_product.PascabayarActivity;
import com.richonmart.adapter.HomeProductAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Category;
import com.richonmart.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductListActivity extends ToolbarActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;

    private Category category;

    private HomeProductAdapter mAdapter;

    private List<Product> products = new ArrayList<>();

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_product_list;
    }

    @Override
    protected void onViewCreated() {
        category = (Category) getIntent().getSerializableExtra("Category");
        tvTitle.setText(category.getTitle());
        rvProduct.setLayoutManager(new LinearLayoutManager(this, GridLayoutManager.VERTICAL, false));
        mAdapter = new HomeProductAdapter(this, 3, new HomeProductAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, final int position, Product product) {
                API.addCartProduct(product);
                Toast.makeText(ProductListActivity.this, "Sukses ditambahkan ke cart", Toast.LENGTH_SHORT).show();
            }
        });
        rvProduct.setAdapter(mAdapter);
        getProduct();
    }

    private void getProduct() {
        API.service().getProductByCategory(category.getId()).enqueue(new APICallback<APIResponse>(ProductListActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                ProductListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        products = apiResponse.getData().getProducts();
                        mAdapter.setItems(products);
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                ProductListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ProductListActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

}
