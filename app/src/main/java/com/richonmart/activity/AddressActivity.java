package com.richonmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonElement;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Address;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

import java.util.Map;
import java.util.Set;

public class AddressActivity extends ToolbarActivity implements OnMapReadyCallback {


    private static final int PLACE_PICKER_REQUEST = 1;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.etPhoneNumber)
    TextView etPhoneNumber;

    @BindView(R.id.etNamePlace)
    EditText etNamePlace;

    @BindView(R.id.etReceiverName)
    EditText etReceiverName;

    @BindView(R.id.etProvince)
    EditText etProvince;

    @BindView(R.id.etCity)
    EditText etCity;

    @BindView(R.id.etKecamatan)
    EditText etKecamatan;

    @BindView(R.id.etPostCode)
    EditText etPostCode;

    @BindView(R.id.etFullAddress)
    EditText etFullAddress;

//    @BindView(R.id.map)
    SupportMapFragment mapFragment;

    private GoogleMap mMap;

    private double lat = 0;

    private double lng = 0;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_address;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.tambah_alamat));
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setAllGesturesEnabled(false);
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 14.0f ) );
    }

    @OnClick(R.id.rlMapView)
    void mapViewClicked() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            // for activty
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            // for fragment
            //startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnSave)
    void btnSave() {
        if (lat == 0) {
            showError("Silahkan pilih lokasi anda di Map terlebih dahulu");
            return;
        }
        runOnUiThread(new Runnable() {
            public void run() {
                Extension.showLoading(AddressActivity.this);
            }
        });

        Address address = new Address();
        address.setName(etNamePlace.getText().toString());
        address.setRecipientName(etReceiverName.getText().toString());
        address.setProvince(etProvince.getText().toString());
        address.setCity(etCity.getText().toString());
        address.setSubDistrict(etKecamatan.getText().toString());
        address.setPostalCode(etPostCode.getText().toString());
        address.setPhoneNumber("+62"+etPhoneNumber.getText().toString());
        address.setFullAddress(etFullAddress.getText().toString());
        address.setLat(lat);
        address.setLng(lng);

        API.service().createAddress(address).enqueue(new APICallback<APIResponse>(AddressActivity.this) {
            @Override
            protected void onSuccess(APIResponse response) {
                AlertDialog alertDialog = new AlertDialog.Builder(AddressActivity.this).create();
                alertDialog.setMessage("Alamat telah sukses ditambah!");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                onBackPressed();
                            }
                        });
                alertDialog.show();
            }

            @Override
            protected void onError(BadRequest error) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                    }
                });

                if (error.code == 400) {
                    AlertDialog alertDialog = new AlertDialog.Builder(AddressActivity.this).create();
                    alertDialog.setTitle(getString(R.string.sorry));
                    alertDialog.setMessage(error.errorDetails);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    try {
                        Set<Map.Entry<String, JsonElement>> entries = error.errors.entrySet();//will return members of your object
                        for (Map.Entry<String, JsonElement> entry : entries) {
                            if (entry.getKey().matches("name")) {
                                etNamePlace.setError(" " + entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("recipient_name")) {
                                etReceiverName.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("province")) {
                                etProvince.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("city")) {
                                etCity.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("sub_district")) {
                                etKecamatan.setError(" " + entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("postal_code")) {
                                etPostCode.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("full_address")) {
                                etFullAddress.setError(entry.getValue().getAsString());
                            }
                        }

                    } catch (Exception exception) {
                        Log.e("loginAPI", "" + exception);
                        AlertDialog alertDialog = new AlertDialog.Builder(AddressActivity.this).create();
                        alertDialog.setTitle(getString(R.string.sorry));
                        alertDialog.setMessage(error.errorDetails);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                mapFragment.getView().setVisibility(View.VISIBLE);
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()));
                String currentString = place.getAddress().toString();
                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;
                String[] separated = currentString.split(",");
                if (separated.length > 6) {
                    etCity.setText(separated[5]);
                    etFullAddress.setText(separated[0]);
                    etProvince.setText(separated[6]);
                    etKecamatan.setText(separated[4]);
                }
                Log.e("12dwdqwdd", ""+separated[0]);
//                Log.e("adasdada",""+ place.getAddress());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera( CameraUpdateFactory.zoomTo( 14.0f ) );
//                place.get
            }
        }
    }

    private void showError(String text) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(text);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}
