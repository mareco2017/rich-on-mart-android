package com.richonmart.activity;

import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.utils.JavaScriptInterface;

public class WebViewActivity extends ToolbarActivity {

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.tvTitle)
    TextView tvTittle;

    private String type = "pay";
    public int iuranId = 0;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_webview;
    }

    @Override
    protected void onViewCreated() {
        if (getIntent().getStringExtra("TYPE") != null) {
            type = getIntent().getStringExtra("TYPE");
        }
        iuranId = getIntent().getIntExtra("IURAN_ID", 0);
        String url = "";
        String token = MarecoAPI.getToken();
        JavaScriptInterface jsInterface;

        if (type.equals("atb") || type.equals("pulsa") || type.equals("paket")|| type.equals("3") || type.equals("1") || type.equals("4") || type.equals("5") || type.equals("rom")) {
            String price = "" + getIntent().getDoubleExtra("PRICE", 0);
            String desc = getIntent().getStringExtra("DESC").replace(" " , "%20");
            String receiverNumber = "+6285769929777";
            receiverNumber = receiverNumber.replace("+", "%2B");

            if (type.equals("atb") || type.equals("rom")) {
                jsInterface = new JavaScriptInterface(this, type , getIntent().getIntExtra("ID", 0));
            }
            else if (type.equals("3") || type.equals("1") || type.equals("4") || type.equals("5")) {
                jsInterface = new JavaScriptInterface(this, type , getIntent().getIntExtra("ID", 0));
            }
            else {
                jsInterface = new JavaScriptInterface(this, type , getIntent().getIntExtra("ID", 0), getIntent().getStringExtra("PHONE_NUMBER"));
            }

            webView.addJavascriptInterface(jsInterface, "JSInterface");
            url = MarecoAPI.getURL()+"/webview/user/pay?price=" + price + "&title=Rich%20On%20Mart&desc="+ desc + "&receiver_phone_number=" + receiverNumber + "&token=" + token;
        }
        else if (type.equals("mareco_syarat")) {
            tvTittle.setText(R.string.terms);
            tvTittle.setVisibility(View.VISIBLE);
            url = MarecoAPI.getURL()+"/terms-conditions";
        }
        else if (type.equals("mareco_kebijakan")) {
            tvTittle.setText(R.string.privacy_policy);
            tvTittle.setVisibility(View.VISIBLE);
            url = MarecoAPI.getURL()+"/privacy-policy";
        }
        else if (type.equals("top_up_instruction")) {
            tvTittle.setText(R.string.instruction_top_up);
            tvTittle.setVisibility(View.VISIBLE);
            url = "https://mart.richonpay.com/webview/cara-isi-saldo";
        }
        else {
            url = MarecoAPI.getURL()+"/webview/user/top-up?token=" + token;
        }

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
//                return super.shouldOverrideUrlLoading(view, request);
                view.loadUrl(request);
                return true;
            }
        });

    }
}