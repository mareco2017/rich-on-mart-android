package com.richonmart.activity;

import android.widget.TextView;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.Bill;
import com.richonmart.model.Order;
import com.richonmart.utils.Extension;

import java.util.List;

public class WaterBillDetailActivity extends ToolbarActivity {

    @BindView(R.id.tvTotalDenda)
    TextView tvTotalDenda;

    @BindView(R.id.tvTagihanNonAir)
    TextView tvTagihanNonAir;

    @BindView(R.id.tvStampFee)
    TextView tvStampFee;

    @BindView(R.id.tvAdmin)
    TextView tvAdmin;

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;

    @BindView(R.id.tvMeterAkhir)
    TextView tvMeterAkhir;

    @BindView(R.id.tvMeterAwal)
    TextView tvMeterAwal;

    private Bill bill;

    private Order order;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_atb_detail;
    }

    @Override
    protected void onViewCreated() {
        bill = (Bill) getIntent().getSerializableExtra("BILL");
        order = (Order) getIntent().getSerializableExtra("ORDER");
        double price = 0;
        double admin = 0;
        double denda = 0;
        double tagihanNonAir = 0;
        double meterAwal = 0;
        double meterAkhir = 0;
        List<Bill> bills = order.getOptions().getInquiry().getBill();
        for (int l = 0; l < bills.size(); l++) {
            price += bills.get(l).getNilaiTagihan();
            admin += bills.get(l).getAdmin();
            denda += bills.get(l).getPenalty();
            tagihanNonAir += bills.get(l).getTagihanLain();
            meterAwal += bills.get(l).getMeterAwal();
            meterAkhir += bills.get(l).getMeterAkhir();
        }
        tvTotalDenda.setText(Extension.priceFormat(denda));
        tvTagihanNonAir.setText(Extension.priceFormat(tagihanNonAir));
        tvStampFee.setText(Extension.priceFormat(bill.getFee()));
        tvAdmin.setText(Extension.priceFormat(admin));
        tvTotalPrice.setText(Extension.priceFormat(order.getTotal()));
        tvTotalBill.setText(Extension.priceFormat(price));
        tvMeterAkhir.setText(Extension.priceFormat(meterAwal));
        tvMeterAwal.setText(Extension.priceFormat(meterAkhir));
    }

}