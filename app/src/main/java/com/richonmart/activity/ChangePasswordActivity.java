package com.richonmart.activity;

import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;

public class ChangePasswordActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.etPassword)
    EditText etPassword;

    @BindView(R.id.etPasswordRepeat)
    EditText etPasswordRepeat;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_change_password;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(getString(R.string.ubah_kata_sandi));
    }

    @OnClick(R.id.btnSave)
    void onSaveClicked() {
        if (etPassword.getText().toString().length() < 6) {
            Toast.makeText(this, R.string.please_insert_valid_password, Toast.LENGTH_SHORT).show();
        }
        else if (etPassword.getText().toString().equals(etPasswordRepeat.getText().toString())) {
            Intent intent = new Intent(this, PasswordConfirmationActivity.class);
            intent.putExtra("TYPE", "change_password");
            intent.putExtra("PASSWORD", etPassword.getText().toString());
            startActivity(intent);
        }
        else {
            Toast.makeText(this, R.string.passwords_dont_match, Toast.LENGTH_SHORT).show();
        }
    }

}
