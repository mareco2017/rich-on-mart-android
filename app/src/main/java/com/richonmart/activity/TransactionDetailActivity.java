package com.richonmart.activity;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.Bill;
import com.richonmart.model.Inquiry;
import com.richonmart.model.Order;
import com.richonmart.utils.Extension;

public class TransactionDetailActivity  extends ToolbarActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvInvoiceNo)
    TextView tvInvoiceNo;

    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.tvDeliveryType)
    TextView tvDeliveryType;

    @BindView(R.id.tvDate)
    TextView tvDate;

    @BindView(R.id.tvTotalPay)
    TextView tvTotalPay;

    private Order order;

    private int type;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_transaction_detail;
    }

    @Override
    protected void onViewCreated() {
        type = getIntent().getIntExtra("TYPE", 11);
        order = (Order) getIntent().getSerializableExtra("Order");
        tvTitle.setText(order.getReferenceNumber());
        tvInvoiceNo.setText(order.getReferenceNumber());
        tvDeliveryType.setText(order.getOptions().getDeliveryType() == 0 ? "Instant" : "Regular");
        tvTotalPay.setText(Extension.priceFormat(order.getTotal()));
        tvDate.setText(Extension.receiptDetailDateFormat.format(order.getCreatedAt()));
        if (order.getStatus() == 20) {
            tvStatus.setText("Pesanan Terkonfirmasi");
        }
        if (order.getStatus() == 21) {
            tvStatus.setText("Pesanan Dibelanja");
        }
        if (order.getStatus() == 22) {
            tvStatus.setText("Pesanan Diantar");
        }
        if (order.getStatus() == 3) {
            tvStatus.setText("Pesanan Selesai");
        }
    }
}
