package com.richonmart.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Activate;
import com.richonmart.model.User;
import com.richonmart.utils.Extension;

public class ActivateMarecoActivity extends ToolbarActivity {

    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_activate_mareco;
    }

    @Override
    protected void onViewCreated() {
        User user = API.currentUser();
//        String number = wargaModel.getWargaTelpon();
        tvPhoneNumber.setText( user.getPhoneNumber());
    }

    @OnClick(R.id.btnStart)
    void callActivateAPI() {
        User user = API.currentUser();
        Extension.showLoading(this);
        MarecoAPI.service().activate(new Activate(user.getPhoneNumber(),user.getFullname(),user.getEmail())).enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {
                Extension.dismissLoading();
                Intent intent = new Intent(ActivateMarecoActivity.this, OTPVerificationActivity.class);
                startActivity(intent);
                finish();
            }
            @Override
            protected void onError(BadRequest error) {
                Extension.dismissLoading();
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivateMarecoActivity.this);
                builder.setTitle(getString(R.string.sorry))
                        .setMessage(error.getErrorDetails())
                        .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                builder.show();
            }
        });
    }


}