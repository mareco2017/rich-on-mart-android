package com.richonmart.activity;

import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.adapter.ViewPagerAdapter;
import com.richonmart.api.API;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.fragment.*;
import com.richonmart.utils.LockableViewPager;

public class MainActivity extends ToolbarActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.viewPager)
    LockableViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    private boolean doubleBackToExitPressedOnce = false;
    private int selectedTab = 0;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewCreated() {
        initView();
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
    }

    private void initView() {
        revertStatusBar();
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setClipToPadding(false);
        viewPager.setPadding(0, 0, 0, 0);
        viewPager.setPageMargin(0);
        viewPager.setOffscreenPageLimit(8);
        viewPagerAdapter.addPage(new HomeFragment(), getString(R.string.home));
        viewPagerAdapter.addPage(new TransactionFragment(), "Pesanan");
        viewPagerAdapter.addPage(new CartFragment(), getString(R.string.cart));
        if (API.isLoggedIn()) {
            viewPagerAdapter.addPage(new AccountFragment(), getString(R.string.login));
        }
        else {
            viewPagerAdapter.addPage(new LoginFragment(), getString(R.string.account));
        }

//        viewPagerAdapter.addPage(profileFragment, getString(R.string.account));
        viewPager.setSwipeable(false);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        tabLayout.setPadding(0, 0, 0, 0);
        tabLayout.setTabTextColors(ContextCompat.getColor(this, R.color.textGray), ContextCompat.getColor(this, R.color.primary));
        tabLayout.removeAllTabs();

        View view1 = getLayoutInflater().inflate(R.layout.tab_bar, null);
        TextView tvTab1 = view1.findViewById(R.id.title);
        tvTab1.setText(R.string.home);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view1));

        View view4 = getLayoutInflater().inflate(R.layout.tab_bar, null);
        TextView tvTab4 = view4.findViewById(R.id.title);
        tvTab4.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColorMain));
        tvTab4.setText("Pesanan");
        tabLayout.addTab(tabLayout.newTab().setCustomView(view4));

        View view3 = getLayoutInflater().inflate(R.layout.tab_bar, null);
        TextView tvTab3 = view3.findViewById(R.id.title);
        tvTab3.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColorMain));
        tvTab3.setText(R.string.cart);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view3));

        View view2 = getLayoutInflater().inflate(R.layout.tab_bar, null);
        TextView tvTab2 = view2.findViewById(R.id.title);
        tvTab2.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColorMain));
        tvTab2.setText(API.isLoggedIn() ? R.string.account : R.string.login);
        tabLayout.addTab(tabLayout.newTab().setCustomView(view2));



        changeTabSelection(0);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                changeTabSelection(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                TextView tvTab = tabLayout.getTabAt(position).getCustomView().findViewById(R.id.title);
                tvTab.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.textColorMain));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                changeTabSelection(position);
            }
        });


        try {
            if (selectedTab != 0) {
                if (tabLayout.getTabAt(selectedTab) != null) {
                    tabLayout.getTabAt(selectedTab).select();
                }
                selectedTab = 0;
            }


        } catch (Exception exception) {
            Log.e("ERROR", "" + exception);
        }

    }

    private void changeTabSelection(int position) {
        try {
            TabLayout.Tab tab0 = tabLayout.getTabAt(0);
            TabLayout.Tab tab1 = tabLayout.getTabAt(1);
            TabLayout.Tab tab2 = tabLayout.getTabAt(2);

            TabLayout.Tab tab3 = tabLayout.getTabAt(3);

            if (tab0 != null && tab0.getCustomView() != null) {
                tab0.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_home_inactive);
            }

            if (tab1 != null  && tab1.getCustomView() != null) {
                tab1.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_order_inactive);
            }

            if (tab2 != null  && tab2.getCustomView() != null) {
                tab2.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_cart_inactive);
            }

            if (tab3 != null  && tab3.getCustomView() != null) {
                tab3.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_account_inactive);
            }

            switch (position) {
                case 0:
                    if (tab0 != null) {
                        tab0.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_home);
                        revertStatusBar();
                    }
                    break;
                case 1:
                    if (tab1 != null) {
                        tab1.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_order);
                        addToolbarBackground();
                    }
                    break;
                case 2:
                    if (tab2 != null) {
                        tab2.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_cart);
                        addToolbarBackground();
                    }
                    break;
                case 3:
                    if (tab3 != null) {
                        tab3.getCustomView().findViewById(R.id.icon).setBackgroundResource(R.drawable.tab_account);
                        addToolbarBackground();
                    }
                    break;

                default:
                    break;
            }
            TextView tvTab = tabLayout.getTabAt(position).getCustomView().findViewById(R.id.title);
            tvTab.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.primary));
        } catch (Exception exception) {
            Log.e("ERROR", "CHANGE BOTTOM BAR ICON " + exception);
        }
    }

    private void addToolbarBackground() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
//        ivLogo.setVisibility(View.GONE);
    }

    private void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    private void revertStatusBar() {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        tvTitle.setText("");
//        ivLogo.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.finishAffinity();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
