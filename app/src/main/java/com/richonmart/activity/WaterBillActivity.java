package com.richonmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.activity.payment_product.MobileCreditListActivity;
import com.richonmart.activity.payment_product.PaymentHistoryActivity;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.*;
import com.richonmart.utils.Extension;
import com.richonmart.utils.LockableViewPager;

import java.util.Timer;
import java.util.TimerTask;

public class WaterBillActivity extends ToolbarActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.etCustomerNumber)
    EditText etCustomerNumber;

    @BindView(R.id.etAmount)
    EditText etAmount;

    @BindView(R.id.tvCustomerNumber)
    TextView tvCustomerNumber;

    @BindView(R.id.tvCustomerResi)
    TextView tvCustomerResi;

    @BindView(R.id.tvCustomerName)
    TextView tvCustomerName;

    @BindView(R.id.tvTotalBill)
    TextView tvTotalBill;

    @BindView(R.id.tvPeriode)
    TextView tvPeriode;

    @BindView(R.id.tvTotalPrice)
    TextView tvTotalPrice;

    @BindView(R.id.tvTotalPayment)
    TextView tvTotalPayment;

    @BindView(R.id.llInformation)
    LinearLayout llInformation;

    @BindView(R.id.llBill)
    LinearLayout llBill;

    @BindView(R.id.btnPay)
    Button btnPay;

    @BindView(R.id.llWallet)
    LinearLayout llWallet;

    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private Timer timer;
    private String customerID = "";
    public static boolean isPaymentSuccess = false;
    Integer selectedID = 0;
    int id;
    Bill bill;
    Order order;
    Option option;
    Inquiry inquiry;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_atb;
    }

    @Override
    protected void onViewCreated() {
        isPaymentSuccess = false;
        tvTitle.setText(getString(R.string.atb));
        etCustomerNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(final Editable s) {
                customerID = s.toString();
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        // do your actual work here
                        if (TextUtils.isEmpty(s.toString().trim())) {
                            requestInquiry();
                        } else {
                            requestInquiry();
                        }
                    }
                }, 600);
            }
        });

        Extension.setupWallet(llWallet,tvBalance, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaymentSuccess) {
            resetBill();
        }
    }

    @OnClick(R.id.tvDetail)
    void detailClicked() {
        Intent intent = new Intent(this, WaterBillDetailActivity.class);
        intent.putExtra("BILL", bill);
        intent.putExtra("ORDER", order);
        startActivity(intent);
    }

    @OnClick(R.id.llDropdown)
    void toSelectProducts() {
        Intent intent = new Intent(this, MobileCreditListActivity.class);
        intent.putExtra("TYPE", String.valueOf(0));
        intent.putExtra("VIEWTYPE", 0);
        startActivityForResult(intent, 1);
    }

    @OnClick(R.id.btnPay)
    void payBtnClicked() {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("PRICE", order.getTotal());
        intent.putExtra("DESC", "AIR ATB " + inquiry.getIdpel());
        intent.putExtra("TYPE", "atb");
        intent.putExtra("ID", id);
        startActivity(intent);
    }

    public void resetBill() {
        llBill.setVisibility(View.GONE);
        llInformation.setVisibility(View.GONE);
        btnPay.setEnabled(false);
        btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_dark_gray);
    }

    private void requestInquiry() {
        try {
            if (selectedID != 0 && customerID.length() > 3) {

//                WaterbillActivity.this.runOnUiThread(new Runnable() {
//                    public void run() {
//                        progressBar.setVisibility8787(View.VISIBLE);
//                    }
//                });

                PaymentProductBody inquiryBody = new PaymentProductBody();
                inquiryBody.setId(selectedID);
                inquiryBody.setCustomerId(customerID);

                API.service().inquiryATB(inquiryBody).enqueue(new APICallback<APIResponse>(WaterBillActivity.this) {
                    @Override
                    protected void onSuccess(final APIResponse apiResponse) {
                        id = apiResponse.getData().getOrder().getId();
                        order = apiResponse.getData().getOrder();
                        inquiry = apiResponse.getData().getOrder().getOptions().getInquiry();
                        WaterBillActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                setupView(apiResponse.getData().getOrder());
                            }
                        });
                    }

                    @Override
                    protected void onError(final BadRequest error) {
                        WaterBillActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                resetBill();
                                Toast.makeText(WaterBillActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        } catch (Exception exception) {
            Log.e("requestInquiry", "" + exception);
        }
    }

    private void setupView(Order order) {
        llBill.setVisibility(View.VISIBLE);
        llInformation.setVisibility(View.VISIBLE);
        tvCustomerNumber.setText(customerID);
        btnPay.setEnabled(true);
        btnPay.setBackgroundResource(R.drawable.bg_rounded_corner_orange);

        Option options = order.getOptions();
        Inquiry inquiry = options.getInquiry();

        tvCustomerResi.setText(inquiry.getRefID());
        tvCustomerName.setText(inquiry.getName());

        Bill bill = inquiry.getBill().get(0);
        this.bill = bill;
        tvTotalBill.setText(bill.getJumlahTagihan());
        tvPeriode.setText(bill.getPeriode());
        tvTotalPrice.setText(Extension.numberPriceFormat(order.getTotal()));

        tvTotalPayment.setText(Extension.priceFormat(order.getTotal()));
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.history, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                selectedID = data.getIntExtra("product_id", 0);
                etAmount.setText(data.getStringExtra("product_name"));
                requestInquiry();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history: {
                Intent intent = new Intent(WaterBillActivity.this, PaymentHistoryActivity.class);
                intent.putExtra("TYPE", 11);
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}