package com.richonmart.activity;

import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.base.ToolbarActivity;

public class MainMarecoActivity extends ToolbarActivity {
    @BindView(R.id.tv_deal_cashless)
    TextView tvDealCashless;
    View view;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_mareco_main;
    }

    @Override
    protected void onViewCreated() {
        clickedWord();
    }

    void clickedWord() {

        setupTextView(tvDealCashless, "Syarat dan Ketentuan", "Kebijakan Privasi.");
        tvDealCashless.setHighlightColor(Color.TRANSPARENT);
    }

    private void setupTextView(TextView textView, final String secondText, String lastText) {

        SpannableStringBuilder spanText = new SpannableStringBuilder();
        String firstText = "Dengan mengaktifkan Mareco Anda telah setuju \ndengan ";
        spanText.append(firstText);
        spanText.append(secondText);
        spanText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                intentPage(1);
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.blue));    // you can use custom color
                textPaint.setUnderlineText(false);    // this remove the underline
            }
        }, spanText.length() - secondText.length(), spanText.length(), 0);
        spanText.append(" & ");
        spanText.append(lastText);
        spanText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                intentPage(2);
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                textPaint.setColor(getResources().getColor(R.color.blue));    // you can use custom color
                textPaint.setUnderlineText(false);    // this remove the underline
            }
        },spanText.length() - lastText.length(), spanText.length(), 0);

        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spanText, TextView.BufferType.SPANNABLE);

    }

    void intentPage(int type) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("TYPE", type == 1 ? "mareco_syarat" : "mareco_kebijakan");
        startActivity(intent);
    }

    @OnClick(R.id.btn_cashless_active)
    void isComeJoin() {
        finish();
    }
}
