package com.richonmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.gson.JsonElement;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.utils.Extension;
import okhttp3.MultipartBody;

import java.util.Map;
import java.util.Set;

public class RegisterActivity extends ToolbarActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.btnRegister)
    Button btnRegister;


    @Override
    protected int getContentViewResource() {
        return R.layout.activity_register;
    }

    @Override
    protected void onViewCreated() {


    }

    @OnClick(R.id.btnRegister)
    void register() {
        try {
            btnRegister.setEnabled(false);
            if (etUsername.getText().toString().length() < 3) {
                showError(getString(R.string.please_insert_valid_name));
                btnRegister.setEnabled(true);
                return;
            }

            if (!Extension.isValidEmail(etEmail.getText().toString())) {
                showError(getString(R.string.please_insert_valid_email));
                btnRegister.setEnabled(true);
                return;
            }

            if (etPhoneNumber.getText().toString().length() < 8) {
                showError(getString(R.string.please_insert_valid_phone));
                btnRegister.setEnabled(true);
                return;
            }

            if (etPassword.getText().toString().length() < 6) {
                Toast.makeText(this, R.string.please_insert_valid_password, Toast.LENGTH_SHORT).show();
                btnRegister.setEnabled(true);
            } else {
                registerValid();
            }
        } catch (Exception exception) {
            Log.e("register", "" + exception);
            btnRegister.setEnabled(true);
        }
    }

    private void registerValid() {

        runOnUiThread(new Runnable() {
            public void run() {
                Extension.showLoading(RegisterActivity.this);
            }
        });
        MultipartBody.Builder buildernew = new MultipartBody.Builder();
        buildernew.setType(MultipartBody.FORM);

        String nameArray[] = etUsername.getText().toString().split(" ", 2);
        buildernew.addFormDataPart("first_name", ((nameArray.length == 2) ? nameArray[0] : etUsername.getText().toString()));
        buildernew.addFormDataPart("last_name", ((nameArray.length == 2) ? nameArray[1] : ""));
        buildernew.addFormDataPart("phone_number", "+62" + etPhoneNumber.getText().toString());
        buildernew.addFormDataPart("email", etEmail.getText().toString());
        buildernew.addFormDataPart("password", etPassword.getText().toString());

        MultipartBody requestBody = buildernew.build();

        API.service().register(requestBody).enqueue(new APICallback<APIResponse>(RegisterActivity.this) {
            @Override
            protected void onSuccess(APIResponse response) {
                API.setToken(response.getData().getToken());
                API.setUser(response.getData().getUser());
                runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                    }
                });
                btnRegister.setEnabled(true);
                startActivity(new Intent(RegisterActivity.this, MainActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }

            @Override
            protected void onError(BadRequest error) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                    }
                });
                btnRegister.setEnabled(true);

                if (error.code == 400) {
                    AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
                    alertDialog.setTitle(getString(R.string.sorry));
                    alertDialog.setMessage(error.errorDetails);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    btnRegister.setEnabled(true);
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    try {
                        Set<Map.Entry<String, JsonElement>> entries = error.errors.entrySet();//will return members of your object
                        for (Map.Entry<String, JsonElement> entry : entries) {
                            if (entry.getKey().matches("first_name") || entry.getKey().matches("last_name")) {
                                etUsername.setError(etUsername.getError().toString() + " " + entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("phone_number")) {
                                etPhoneNumber.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("email")) {
                                etEmail.setError(entry.getValue().getAsString());
                            }
                            if (entry.getKey().matches("password")) {
                                etPassword.setError(entry.getValue().getAsString());
                            }
                        }

                    } catch (Exception exception) {
                        Log.e("loginAPI", "" + exception);
                        AlertDialog alertDialog = new AlertDialog.Builder(RegisterActivity.this).create();
                        alertDialog.setTitle(getString(R.string.sorry));
                        alertDialog.setMessage(error.errorDetails);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        btnRegister.setEnabled(true);
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }
            }
        });

    }

    private void showError(String text) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage(text);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

}