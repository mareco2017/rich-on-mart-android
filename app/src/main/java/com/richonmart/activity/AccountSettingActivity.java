package com.richonmart.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.api.MarecoAPI;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;

public class AccountSettingActivity extends ToolbarActivity {

    @BindView(R.id.tvTitle)
    TextView tvTittle;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_account_setting;
    }

    @Override
    protected void onViewCreated() {
        tvTittle.setText(getString(R.string.pengaturan_akun));
    }

    @OnClick({R.id.llEditProfile})
    void toEditProfile() {
        Intent intent = new Intent(AccountSettingActivity.this, EditProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llChangePassword)
    void toChangePassword() {
        Intent intent = new Intent(AccountSettingActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llAddress)
    void toAddresses() {
        Intent intent = new Intent(AccountSettingActivity.this, AddressListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.llLogout)
    void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AccountSettingActivity.this);
        builder.setTitle(getString(R.string.logout_desc))
                .setPositiveButton(getString((R.string.dialog_ok)), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        API.setToken(null);
                        MarecoAPI.setUser(null);
                        MarecoAPI.setToken(null);
                        MarecoAPI.setLogInStatus(false);
                        dialog.dismiss();
                        callLogOutAPI();
                        Intent intent = new Intent(AccountSettingActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);;
                        finish();
                    }
                }).setNegativeButton(getString(R.string.batal), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }

    private void callLogOutAPI() {

        API.service().logout().enqueue(new APICallback<APIResponse>(this) {
            @Override
            protected void onSuccess(APIResponse apiResponse) {

            }

            @Override
            protected void onError(BadRequest error) {

            }
        });
    }
}
