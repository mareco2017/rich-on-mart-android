package com.richonmart.activity;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import com.richonmart.R;
import com.richonmart.adapter.CategoryProductAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Category;

public class CategoryListActivity extends ToolbarActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;


    private CategoryProductAdapter categoryAdapter;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_category_list;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(R.string.kategori);
        rvCategory.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false));
        categoryAdapter = new CategoryProductAdapter(this, false, new CategoryProductAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position, Category category) {
                Intent intent = new Intent(CategoryListActivity.this, ProductListActivity.class);
                intent.putExtra("Category", category);
                startActivity(intent);
            }
        });
        rvCategory.setAdapter(categoryAdapter);
        getCategory();
    }

    private void getCategory() {
        API.service().getCategory().enqueue(new APICallback<APIResponse>(CategoryListActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                CategoryListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        categoryAdapter.setItems(apiResponse.getData().getCategories());
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                CategoryListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(CategoryListActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

}
