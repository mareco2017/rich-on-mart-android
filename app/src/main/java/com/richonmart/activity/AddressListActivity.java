package com.richonmart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.richonmart.R;
import com.richonmart.adapter.AddressAdapter;
import com.richonmart.adapter.CategoryProductAdapter;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.base.ToolbarActivity;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Address;
import com.richonmart.model.Category;
import com.richonmart.utils.Extension;

import java.util.List;

public class AddressListActivity extends ToolbarActivity {


    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.rvCategory)
    RecyclerView rvCategory;

    boolean isFromCart = false;

    private AddressAdapter addressAdapter;

    private List<Address> addresses;

    @Override
    protected int getContentViewResource() {
        return R.layout.activity_address_list;
    }

    @Override
    protected void onViewCreated() {
        tvTitle.setText(R.string.alamat);
        isFromCart = getIntent().getBooleanExtra("FROMCART", false);
        rvCategory.setLayoutManager(new LinearLayoutManager(this, GridLayoutManager.VERTICAL, false));
        addressAdapter = new AddressAdapter(this, isFromCart, new AddressAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position, Address address) {
                if (!isFromCart) {
                    moreBtnAction(address.getId(),position);
                }
                else {
                    Intent intent = new Intent();
                    intent.putExtra("ADDRESS", address);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
        rvCategory.setAdapter(addressAdapter);
        getAddress();
    }

    private void moreBtnAction(int id, int position) {
        final CharSequence[] items = {"Jadikan Alamat utama", "Hapus Alamat ini", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Jadikan Alamat utama")) {
                    setPrimary(id);
                } else if (items[item].equals("Hapus Alamat ini")) {
                    removeAddress(id, position);
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void setPrimary(int id) {
        runOnUiThread(new Runnable() {
            public void run() {
                Extension.showLoading(AddressListActivity.this);
            }
        });
        API.service().setPrimaryAddress(id).enqueue(new APICallback<APIResponse>(AddressListActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                AddressListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                        for(int l=0; l<addresses.size(); l++){
                            if (addresses.get(l).getId() == id) {
                                addresses.get(l).setPrimary(1);
                            }
                            else {
                                addresses.get(l).setPrimary(0);
                            }
                        }
                        addressAdapter.setItems(addresses);
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                AddressListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                        Toast.makeText(AddressListActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void removeAddress(int id, int position) {
        runOnUiThread(new Runnable() {
            public void run() {
                Extension.showLoading(AddressListActivity.this);
            }
        });
        API.service().removeAddress(id).enqueue(new APICallback<APIResponse>(AddressListActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                AddressListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                        addresses.remove(position);
                        addressAdapter.setItems(addresses);
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                AddressListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Extension.dismissLoading();
                        Toast.makeText(AddressListActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getAddress();
    }

    private void getAddress() {
        API.service().getAddress().enqueue(new APICallback<APIResponse>(AddressListActivity.this) {
            @Override
            protected void onSuccess(final APIResponse apiResponse) {
                AddressListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        addresses = apiResponse.getData().getAddresses();
                        addressAdapter.setItems(apiResponse.getData().getAddresses());
                    }
                });
            }

            @Override
            protected void onError(final BadRequest error) {
                AddressListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(AddressListActivity.this, error.errorDetails, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @OnClick(R.id.btnAdd)
    void addAddress() {
        Intent intent = new Intent(AddressListActivity.this, AddressActivity.class);
        startActivity(intent);
    }

}
