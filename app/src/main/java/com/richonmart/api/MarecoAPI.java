package com.richonmart.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;
import com.richonmart.model.User;
import com.richonmart.utils.DateDeserializer;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MarecoAPI {
    private static final String TOKEN = "MARECO_TOKEN";
    private static final String RESET_PASS = "RESET_PASS";
    private static final String OTP_SUCESS = "OTP_SUCESS";
    private static final String USER = "MARECO_USER";
    private static final String isLoggedIn = "isLoggedIn";
    private static final String CASHIER_MODEL = "CASHIER_MODEL";
    private static final String URL = "https://richonpay.com";
    private static APIService SERVICE;
    private static boolean sessionError = false;
    private static Converter<ResponseBody, BadRequest> ERROR_CONVERTER;
    private static boolean ignoreToken = false;


    public static APIService service() {
        ignoreToken = false;
        if (SERVICE == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            String token = Hawk.get(TOKEN, "");
                            if (token.equals("")) {
                                ignoreToken = true;
                            }
                            String bearer = "Bearer ";

                            Request original = chain.request();

                            try {
                                if (ignoreToken) {
                                    Request request = original.newBuilder()
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Accept", "application/json")
                                            .method(original.method(), original.body())
                                            .build();
                                    return chain.proceed(request);
                                } else {
                                    Request request = original.newBuilder()
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Accept", "application/json")
                                            .addHeader("Authorization", bearer + token)
                                            .method(original.method(), original.body())
                                            .build();
                                    return chain.proceed(request);
                                }
                            } catch (Exception exception) {
                                Request request = original.newBuilder()
                                        .addHeader("Content-Type", "application/json")
                                        .addHeader("Accept", "application/json")
                                        .method(original.method(), original.body())
                                        .build();
                                return chain.proceed(request);
                            }
                        }
                    })
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .registerTypeAdapter(Date.class, new DateDeserializer())
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl(URL+"/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ERROR_CONVERTER = retrofit.responseBodyConverter(BadRequest.class, new Annotation[0]);
            SERVICE = retrofit.create(APIService.class);
        }
        return SERVICE;
    }

    static Converter<ResponseBody, BadRequest> getErrorConverter() {
        return ERROR_CONVERTER;
    }

    public static void setToken(String token) {
        Hawk.put(TOKEN, token);
    }

    public static String getToken() {
        return Hawk.get(TOKEN, "");
    }

    public static void setOtpSucess(String status) {
        Hawk.put(OTP_SUCESS, status);
    }

    public static void setUser(User user) {
        Hawk.put(USER, user);
    }

    public static User getUser() {
        return Hawk.get(USER);
    }

    public static void setLogInStatus(boolean status) {
        Hawk.put(isLoggedIn, status);
    }

    public static boolean isLoggedIn() {

        if (Hawk.get(isLoggedIn) == null) {
            return false;
        }
        return Hawk.get(isLoggedIn);
    }

    public static void logOut() {
//        MarecoAPI.isAuthorized = false;
        API.setToken("");
        Hawk.deleteAll();

    }

    public static String getURL() {
        return URL;
    }

    public static boolean isOTPSucess() {
        return Hawk.get(OTP_SUCESS) != null;
    }

    public static boolean isSessionError() {
        return sessionError;
    }

    public static void setSessionError(boolean sessionError) {
        MarecoAPI.sessionError = sessionError;
    }
}