package com.richonmart.api;

import com.richonmart.model.*;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface APIService {

    //////////////////////////  POST  //////////////////////////
    @POST("v1/login")
    Call<APIResponse> login(@Body Login login);

    @POST("v1/forgot-password")
    Call<APIResponse> forgotPassword(@Body ForgotPassword forgotPassword);

    @POST("v1/address/create")
    Call<APIResponse> createAddress(@Body Address address);

    @POST("v1/register")
    Call<APIResponse> register(@Body RequestBody requestBody);

    @POST("v1/payment/pdam/inquiry")
    Call<APIResponse> inquiryATB(@Body PaymentProductBody requestBody);

    @POST("v1/payment/pdam/{id}/payment")
    Call<APIResponse> paymentPDAM(@Path("id") int id);

    @POST("v1/payment/telkom/{id}/payment")
    Call<APIResponse> paymentTelkom(@Path("id") int id);

    @GET("v1/home")
    Call<APIResponse> getHomeBanner();

    @POST("v1/process-checkout")
    Call<APIResponse> checkout(@Body Checkout checkout);

    @POST("v1/checkout")
    Call<APIResponse> checkoutPay(@Body Checkout checkout);

    @POST("v1/payment/telepon/{id}/payment")
    Call<APIResponse> paymentTelepon(@Path("id") int id);

    @GET("v1/{id}/products")
    Call<APIResponse> getProductByCategory(@Path("id") int id);

    @GET("v1/category")
    Call<APIResponse> getCategory();

    @GET("v1/address")
    Call<APIResponse> getAddress();

    @POST("v1/address/{id}/set-primary")
    Call<APIResponse> setPrimaryAddress(@Path("id") int id);

    @POST("v1/address/{id}/remove")
    Call<APIResponse> removeAddress(@Path("id") int id);

    @POST("v1/payment/internet/{id}/payment")
    Call<APIResponse> paymentInternet(@Path("id") int id);

    @POST("v1/payment/paket/payment")
    Call<APIResponse> paymentPacketData(@Body RequestBody requestBody);

    @POST("v1/payment/telkom/inquiry")
    Call<APIResponse> telkomInquiry(@Body RequestBody requestBody);

    @POST("v1/payment/listrik/inquiry")
    Call<APIResponse> plnInquiry(@Body RequestBody requestBody);

    @POST("v1/payment/listrik/{id}/payment")
    Call<APIResponse> paymentPLN(@Path("id") int id);

    @POST("v1/payment/telepon/inquiry")
    Call<APIResponse> teleponInquiry(@Body RequestBody requestBody);

    @POST("v1/payment/internet/inquiry")
    Call<APIResponse> internetInquiry(@Body RequestBody requestBody);

    @POST("v1/payment/pulsa/payment")
    Call<APIResponse> paymentPulsa(@Body RequestBody requestBody);

    @POST("activate")
    Call<APIResponse> activate(@Body Activate activate);

    @POST("verify-otp")
    Call<APIResponse> loginMareco(@Body MarecoLogin activate);

    @POST("otp")
    Call<APIResponse> requestOTP(@Body MarecoLogin activate);

    @POST("user/set-pin")
    Call<APIResponse> setPIN(@Body PIN pin);

    @GET("user/me")
    Call<APIResponse> getMarecoUserProfile();

    @POST("user/initialize-wallets")
    Call<APIResponse> initializeWallet();

    @POST("v1/logout")
    Call<APIResponse> logout();

    @POST("v1/refresh")
    Call<APIResponse> refreshToken();

    @POST("v1/user/update-profile")
    Call<APIResponse> updateProfile(@Body RequestBody user);

    @POST("v1/user/change-password")
    Call<APIResponse> changePassword(@Body RequestBody user);

    @GET("v1/payment")
    Call<APIResponse> getPaymentProductByType(@Query("type") String type, @Query("phone_number") String number, @Query("phone_number_package") String isPackage);

    @GET("v1/order/owned")
    Call<APIResponse> getOrder(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit, @Query("status") int status);

    @GET("v1/order/owned")
    Call<APIResponse> getOrder(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit, @Query("status") String status);

}