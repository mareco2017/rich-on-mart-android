package com.richonmart.api;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;
import com.richonmart.App;
import com.richonmart.model.Login;
import com.richonmart.model.Product;
import com.richonmart.model.User;
import com.richonmart.utils.DateDeserializer;
import okhttp3.*;
//import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class API {
    private static final String TOKEN = "TOKEN";
    private static final String RESET_PASS = "RESET_PASS";
    private static final String OTP_SUCESS = "OTP_SUCESS";
    private static final String USER = "USER";
    private static final String CART = "CART";
    private static final String CASHIER_MODEL = "CASHIER_MODEL";
    private static APIService SERVICE;
    private static boolean sessionError = false;
    private static Converter<ResponseBody, BadRequest> ERROR_CONVERTER;
    private static boolean ignoreToken = false;
    private static boolean onUpdateCart = false;

    public static APIService service() {
        ignoreToken = false;
        if (SERVICE == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            String token = Hawk.get(TOKEN, "");
                            String bearer = "Bearer ";
                            if (token.isEmpty()) {
                                bearer = "";
                            }

                            Request original = chain.request();

                            try {
                                if (ignoreToken) {
                                    Request request = original.newBuilder()
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Accept", "application/json")
                                            .addHeader("Timezone", "GMT+7")
                                            .method(original.method(), original.body())
                                            .build();
                                    return chain.proceed(request);
                                } else {
                                    Request request = original.newBuilder()
                                            .addHeader("Content-Type", "application/json")
                                            .addHeader("Accept", "application/json")
                                            .addHeader("Timezone", "GMT+7")
                                            .addHeader("Authorization", bearer + token)
                                            .method(original.method(), original.body())
                                            .build();
                                    return chain.proceed(request);
                                }
                            } catch (Exception exception) {
                                Request request = original.newBuilder()
                                        .addHeader("Content-Type", "application/json")
                                        .addHeader("Accept", "application/json")
                                        .addHeader("Timezone", "GMT+7")
                                        .addHeader("Authorization", bearer + token)
                                        .method(original.method(), original.body())
                                        .build();
                                return chain.proceed(request);
                            }
                        }
                    })
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .registerTypeAdapter(Date.class, new DateDeserializer())
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(client)
                    .baseUrl("https://mart.richonpay.com" + "/api/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            ERROR_CONVERTER = retrofit.responseBodyConverter(BadRequest.class, new Annotation[0]);
            SERVICE = retrofit.create(APIService.class);
        }
        return SERVICE;
    }

    static Converter<ResponseBody, BadRequest> getErrorConverter() {
        return ERROR_CONVERTER;
    }

    public static void setToken(String token) {
        Hawk.put(TOKEN, token);
    }

    public static String getToken() {
        return Hawk.get(TOKEN, "");
    }

    public static User currentUser() {
        return Hawk.get(USER);
    }

    public static void setUser(User profile) {
        Hawk.put(USER, profile);
    }

    public static void addCartProduct(Product product) {
        List<Product> products = new ArrayList<>();
        if (getCartProduct() != null) {
            products = getCartProduct();
        }
        if (products.size() == 0) {
            products.add(product);
        } else {
            boolean addProduct = true;
            for (int l = 0; l < products.size(); l++) {
                if (products.get(l).getId() == product.getId()) {
                    addProduct = false;
                    products.get(l).setCartQty(products.get(l).getCartQty() + 1);
                }
            }
            if (addProduct) {
                products.add(product);
            }
        }

        Hawk.put(CART, products);
    }

    public static void updateProductQty(Product product, int qty) {
        List<Product> products = API.getCartProduct();
        for (int l = 0; l < products.size(); l++) {
            if (products.get(l).getId() == product.getId()) {
                products.get(l).setCartQty(products.get(l).getCartQty() + qty);
            }
        }
        Hawk.put(CART, products);
    }

    public static void removeCartProduct(int index) {
        List<Product> products = API.getCartProduct();
        products.remove(index);
        Hawk.put(CART, products);
    }

    public static List<Product> getCartProduct() {
        if (Hawk.get(CART) == null) {
            List<Product> products = new ArrayList<>();
            return products;
        }
        return Hawk.get(CART);
    }

    public static void setOtpSucess(String status) {
        Hawk.put(OTP_SUCESS, status);
    }

    public static void logOut() {
        App.isAuthorized = false;
        API.setToken("");
        API.setUser(null);
        MarecoAPI.setUser(null);
        MarecoAPI.setToken(null);
        Hawk.deleteAll();

    }

    public static boolean isLoggedIn() {
        return Hawk.get(TOKEN) != null;
    }

    public static boolean isOTPSucess() {
        return Hawk.get(OTP_SUCESS) != null;
    }

    public static boolean isSessionError() {
        return sessionError;
    }

    public static void setSessionError(boolean sessionError) {
        API.sessionError = sessionError;
    }
}