package com.richonmart.api;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class BadRequest<M> {
    @SerializedName("message")
    public String errorDetails;

    @SerializedName("error_msg")
    private String message;

    @SerializedName("code")
    public int code;
    public JsonObject errors;

    public BadRequest(int code, String message) {
        this.code = code;
        this.errorDetails = message;
    }

    public BadRequest(String message) {
        this.errorDetails = message;
    }

    public JsonObject getErrors() {
        return errors;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setErrors(JsonObject errors) {
        this.errors = errors;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
