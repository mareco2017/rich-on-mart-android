package com.richonmart;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import com.orhanobut.hawk.Hawk;

public class App extends Application {

    public static boolean isAuthorized = false;
    public static App.AuthorizedChangeListener variableChangeListener;

    public static void setVariableChangeListener(App.AuthorizedChangeListener variableChangeListener) {
        App.variableChangeListener = variableChangeListener;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public interface AuthorizedChangeListener {
        void onVariableChanged(boolean isAuthorized);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
    }
}
