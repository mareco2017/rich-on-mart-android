package com.richonmart.model;

import java.util.List;

public class APIResponse {

    private String message;
    private APIModels data;
    private String type;
    private Order order;
    private List<Order> orders;
    private List<PaymentProduct> payments;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public APIModels getData() {
        return data;
    }

    public void setData(APIModels data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public List<PaymentProduct> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentProduct> payments) {
        this.payments = payments;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
