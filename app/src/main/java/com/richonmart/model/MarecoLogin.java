package com.richonmart.model;

public class MarecoLogin {

    private String credential;
    private String pin;

    public MarecoLogin(String credential, String pin) {
        this.credential = credential;
        this.pin = pin;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
