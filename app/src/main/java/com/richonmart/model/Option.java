package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Option implements Serializable {

    private double total;
    private Inquiry inquiry;
    @SerializedName("delivery_type")
    private int deliveryType;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Inquiry getInquiry() {
        return inquiry;
    }

    public void setInquiry(Inquiry inquiry) {
        this.inquiry = inquiry;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }
}
