package com.richonmart.model;


import com.google.gson.annotations.SerializedName;

public class PIN {

    private String pin;
    @SerializedName("pin_confirmation")
    private String pinConfirmation;

    public PIN(String pin, String pinConfirmation) {
        this.pin = pin;
        this.pinConfirmation = pinConfirmation;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPinConfirmation() {
        return pinConfirmation;
    }

    public void setPinConfirmation(String pinConfirmation) {
        this.pinConfirmation = pinConfirmation;
    }
}
