package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

public class PaymentProduct {

    private int id;
    @SerializedName("icon_url")
    private String iconUrl;
    @SerializedName("final_price")
    private double finalPrice;
    private double fee;
    private String title;


    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public int getId() {
        return id;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
