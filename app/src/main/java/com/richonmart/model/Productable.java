package com.richonmart.model;

import java.io.Serializable;

public class Productable implements Serializable {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
