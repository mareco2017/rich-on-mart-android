package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

public class Activate {
    @SerializedName("phone_number")
    private String phoneNumber;
    private String fullname;
    private String email;

    public Activate(String phoneNumber, String fullname, String email){
        this.phoneNumber = phoneNumber;
        this.fullname = fullname;
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

