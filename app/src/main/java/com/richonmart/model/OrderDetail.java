package com.richonmart.model;

import java.io.Serializable;

public class OrderDetail implements Serializable {

    private Productable productable;

    public Productable getProductable() {
        return productable;
    }

    public void setProductable(Productable productable) {
        this.productable = productable;
    }
}
