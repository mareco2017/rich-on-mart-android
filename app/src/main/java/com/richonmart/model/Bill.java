package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bill implements Serializable {
    private String periode;
    private double penalty;
    private double tagihanLain;
    private double nilaiTagihan;
    private double total;
    private double admin;
    private double denda;
    private double fee;
    private String jumlahTagihan;
    private double meterAwal;
    private double meterAkhir;

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }

    public double getTagihanLain() {
        return tagihanLain;
    }

    public void setTagihanLain(double tagihanLain) {
        this.tagihanLain = tagihanLain;
    }

    public double getNilaiTagihan() {
        return nilaiTagihan;
    }

    public void setNilaiTagihan(double nilaiTagihan) {
        this.nilaiTagihan = nilaiTagihan;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAdmin() {
        return admin;
    }

    public void setAdmin(double admin) {
        this.admin = admin;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getJumlahTagihan() {
        return jumlahTagihan;
    }

    public void setJumlahTagihan(String jumlahTagihan) {
        this.jumlahTagihan = jumlahTagihan;
    }

    public double getMeterAwal() {
        return meterAwal;
    }

    public void setMeterAwal(double meterAwal) {
        this.meterAwal = meterAwal;
    }

    public double getMeterAkhir() {
        return meterAkhir;
    }

    public void setMeterAkhir(double meterAkhir) {
        this.meterAkhir = meterAkhir;
    }

    public double getDenda() {
        return denda;
    }

    public void setDenda(double denda) {
        this.denda = denda;
    }
}
