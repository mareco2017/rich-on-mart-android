package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Checkout {

    @SerializedName("delivery_type")
    private int deliveryType;
    @SerializedName("user_address_id")
    private int addressId;
    List<Product> products;
    @SerializedName("order_id")
    private int orderId;

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
