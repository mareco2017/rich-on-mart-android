package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Inquiry implements Serializable {

    private String message;
    @SerializedName("nama")
    private String name;
    private String idpel;
    @SerializedName("tagihan")
    private List<Bill> bill;
    @SerializedName("detilTagihan")
    private List<Bill> billDetail;
    private String refID;
    private String jumlahTagihan;
    private double totalTagihan;
    private double lembarTagihanTotal;
    private String lembarTagihan;
    private String productCode;
    private String tarif;
    private String daya;
    private String subscriberID;

    public List<Bill> getBill() {
        return bill;
    }

    public void setBill(List<Bill> bill) {
        this.bill = bill;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getIdpel() {
        return idpel;
    }

    public void setIdpel(String idpel) {
        this.idpel = idpel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getTotalTagihan() {
        return totalTagihan;
    }

    public void setTotalTagihan(double totalTagihan) {
        this.totalTagihan = totalTagihan;
    }

    public String getJumlahTagihan() {
        return jumlahTagihan;
    }

    public void setJumlahTagihan(String jumlahTagihan) {
        this.jumlahTagihan = jumlahTagihan;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getDaya() {
        return daya;
    }

    public void setDaya(String daya) {
        this.daya = daya;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public List<Bill> getBillDetail() {
        return billDetail;
    }

    public void setBillDetail(List<Bill> billDetail) {
        this.billDetail = billDetail;
    }

    public double getLembarTagihanTotal() {
        return lembarTagihanTotal;
    }

    public void setLembarTagihanTotal(double lembarTagihanTotal) {
        this.lembarTagihanTotal = lembarTagihanTotal;
    }

    public String getLembarTagihan() {
        return lembarTagihan;
    }

    public void setLembarTagihan(String lembarTagihan) {
        this.lembarTagihan = lembarTagihan;
    }

    public String getSubscriberID() {
        return subscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        this.subscriberID = subscriberID;
    }
}
