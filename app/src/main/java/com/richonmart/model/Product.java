package com.richonmart.model;

import com.google.gson.annotations.SerializedName;

public class Product {

    private double price;
    private String name;
    private String title;
    @SerializedName("qty")
    private int cartQty = 1;
    @SerializedName("calculated_price")
    private double calcutaltedPrice;
    @SerializedName("cover_url")
    private String coverUrl;
    private double discount;
    private int id;
    @SerializedName("product_id")
    private int productId;
    @SerializedName("unit_string")
    private String unitString;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCalcutaltedPrice() {
        return calcutaltedPrice;
    }

    public void setCalcutaltedPrice(double calcutaltedPrice) {
        this.calcutaltedPrice = calcutaltedPrice;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUnitString() {
        return unitString;
    }

    public void setUnitString(String unitString) {
        this.unitString = unitString;
    }

    public int getCartQty() {
        return cartQty;
    }

    public void setCartQty(int cartQty) {
        this.cartQty = cartQty;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
