package com.richonmart.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.richonmart.R;
import com.richonmart.activity.MarecoAccountActivity;
import com.richonmart.api.MarecoAPI;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;


public class Extension {

    private static Dialog dialog;
    public static SimpleDateFormat receiptDetailDateFormat = new SimpleDateFormat("dd MMM yyyy - hh:mm a", Locale.getDefault());

    public static void setImageRounded(Context context, ImageView view, String url) {
        try {
            if (url != null) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions = requestOptions.transform(new CenterCrop(), new RoundedCorners(25));
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(view);
            }
        } catch (Exception ex) {
            Log.e("ERROR", "LOAD IMAGE: " + ex);
        }
    }

    public static void setImage(Context context, ImageView view, String url) {
        try {
            if (url != null) {
                Glide.with(context)
                        .load(url)
                        .apply(RequestOptions.centerCropTransform())
                        .into(view);
            }
        } catch (Exception ex) {
            Log.e("ERROR", "LOAD IMAGE: " + ex);
        }
    }

    public static void setImage(Context context, ImageView view, int drawable) {
        try {
            Glide.with(context)
                    .load(drawable)
                    .into(view);
        } catch (Exception ex) {
            Log.e("ERROR", "LOAD IMAGE: " + ex);
        }
    }

    public static String phoneNumberCountryFormat(String number) {
        if (number.substring(0,1).equals("0")) {
            number = "+62"+number.substring(1,number.length());
        }
        return number;
    }

    public static String priceFormat(double price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator('.');
        symbols.setDecimalSeparator(',');
        formatter.setDecimalFormatSymbols(symbols);
        return "Rp " + formatter.format(Math.floor(price));
    }

    public static String numberPriceFormat(double price) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator('.');
        symbols.setDecimalSeparator(',');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(Math.floor(price));
    }

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showLoading(final Activity context) {
        try {
            dialog = new Dialog(context, R.style.darkPopupAnimation);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_progressbar);
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setAttributes(lp);

            final TextView tvLoading = dialog.findViewById(R.id.tvLoading);
            ProgressBar indeterminateBar = dialog.findViewById(R.id.indeterminateBar);
            indeterminateBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.primary), PorterDuff.Mode.SRC_ATOP);
            tvLoading.setText(R.string.loading);

            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (Exception exception) {
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        }
    }

    public static void setupWallet(LinearLayout llWallet, TextView tvBalance, final Activity activity) {
        if(MarecoAPI.isLoggedIn()) {
            llWallet.setVisibility(View.VISIBLE);
            tvBalance.setVisibility(View.VISIBLE);
            tvBalance.setText(Extension.priceFormat(MarecoAPI.getUser().getBalances().getPrimary()));
            llWallet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, MarecoAccountActivity.class);
                    activity.startActivity(intent);
                }
            });
        }
        else {
            llWallet.setVisibility(View.GONE);
            tvBalance.setVisibility(View.GONE);
        }
    }

    public static void dismissLoading() {
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception exception) {
            Log.e("DISMISS LOADING", "" + exception);
        }
    }

}
