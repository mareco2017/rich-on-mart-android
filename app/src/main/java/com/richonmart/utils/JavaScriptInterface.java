package com.richonmart.utils;

import android.app.Activity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import com.richonmart.activity.WaterBillActivity;
import com.richonmart.activity.payment_product.PascabayarActivity;
import com.richonmart.activity.payment_product.PayPLNActivity;
import com.richonmart.activity.payment_product.TelkomPSTNActivity;
import com.richonmart.api.API;
import com.richonmart.api.APICallback;
import com.richonmart.api.BadRequest;
import com.richonmart.model.APIResponse;
import com.richonmart.model.Checkout;
import okhttp3.MultipartBody;

public class JavaScriptInterface {
    private Activity activity;
    private int id;
    private String phoneNumber;
    private String type;

    public JavaScriptInterface(Activity activity, String type, int id, String phoneNumber) {
        this.type = type;
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.activity = activity;
    }

    public JavaScriptInterface(Activity activity, String type, int id) {
        this.type = type;
        this.id = id;
        this.activity = activity;
    }

    @JavascriptInterface
    public void onSuccess(String text) {
        if (type.equals("atb")) {
            API.service().paymentPDAM(id).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                    WaterBillActivity.isPaymentSuccess = true;
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                        }
                    });
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
        if (type.equals("rom")) {
            Checkout checkout = new Checkout();
            checkout.setOrderId(id);
            API.service().checkoutPay(checkout).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {

                        }
                    });
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
        else if (type.equals("1")) {
            API.service().paymentTelkom(id).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                    PascabayarActivity.isPaymentSuccess = true;
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                        }
                    });
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } else if (type.equals("5")) {
            API.service().paymentPLN(id).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                    PayPLNActivity.isPaymentSuccess = true;
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                        }
                    });
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } else if (type.equals("3")) {
            API.service().paymentTelepon(id).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                    PascabayarActivity.isPaymentSuccess = true;
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                        }
                    });
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } else if (type.equals("4")) {
            API.service().paymentInternet(id).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                    PascabayarActivity.isPaymentSuccess = true;
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                        }
                    });
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } else if (type.equals("pulsa")) {
            MultipartBody.Builder buildernew = new MultipartBody.Builder();
            buildernew.setType(MultipartBody.FORM);

            buildernew.addFormDataPart("id", String.valueOf(id));
            buildernew.addFormDataPart("phone_number", phoneNumber);
            MultipartBody requestBody = buildernew.build();

            API.service().paymentPulsa(requestBody).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        } else if (type.equals("paket")) {
            MultipartBody.Builder buildernew = new MultipartBody.Builder();
            buildernew.setType(MultipartBody.FORM);

            buildernew.addFormDataPart("id", String.valueOf(id));
            buildernew.addFormDataPart("phone_number", phoneNumber);
            MultipartBody requestBody = buildernew.build();

            API.service().paymentPacketData(requestBody).enqueue(new APICallback<APIResponse>(activity) {
                @Override
                protected void onSuccess(final APIResponse apiResponse) {
                }

                @Override
                protected void onError(final BadRequest error) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(activity, error.errorDetails, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
//        activity.onBackPressed();
        Log.e("callback", "succes " + text);
    }

    @JavascriptInterface
    public void onFailed(String text) {
        Log.e("callback", "asdasdads " + text);
    }
}
